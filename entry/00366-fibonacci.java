// https://www.lintcode.com/problem/fibonacci/

public class Solution {
    /**
     * @param n: an integer
     * @return: an ineger f(n)
     */
    public int fibonacci(int n) {
        // write your code here
        if (cahce.containsKey(n)) {
            return Solution.cahce.get(n);
        }
        else {
            if (n <= 2) {
                cahce.put(n, n - 1);
                return n - 1;
            }
            else {
                int ret = fibonacci(n - 1) + fibonacci(n - 2);
                cahce.put(n, ret);
                return ret;
            } 
        }
    }
    
    private static HashMap<Integer, Integer> cahce = new HashMap<>();
}