// https://www.lintcode.com/problem/lowercase-to-uppercase/

public class Solution {
    /**
     * @param character: a character
     * @return: a character
     */
    public char lowercaseToUppercase(char character) {
        // write your code here
        if ((character >= 'a') && (character <= 'z')) {
            character = (char)('A' + character - 'a');
        } else if ((character >= 'A') && (character <= 'Z')) {
            character = (char)('a' + character - 'A');
        }
        return character;
    }
}