// https://www.lintcode.com/problem/reverse-3-digit-integer/

public class Solution {
    /**
     * @param number: A 3-digit number.
     * @return: Reversed number.
     */
    public int reverseInteger(int number) {
        // write your code here
        int ret = 0;
        while (number > 0) {
            ret = (ret * 10) + (number % 10);
            number = number / 10;
        }
        return ret;
    }
}