// https://www.lintcode.com/problem/rectangle-area/

public class Rectangle {
    /*
     * Define two public attributes width and height of type int.
     */
    // write your code here
    int height, width;

    /*
     * Define a constructor which expects two parameters width and height here.
     */
    // write your code here
    Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }
    
    /*
     * Define a public method `getArea` which can calculate the area of the
     * rectangle and return.
     */
    // write your code here
    public int getArea() {
        return height * width;
    }
}