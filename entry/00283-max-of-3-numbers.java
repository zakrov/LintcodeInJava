// https://www.lintcode.com/problem/max-of-3-numbers/

public class Solution {
    /**
     * @param num1: An integer
     * @param num2: An integer
     * @param num3: An integer
     * @return: an interger
     */
    public int maxOfThreeNumbers(int num1, int num2, int num3) {
        // write your code here
        return Math.max(num1, Math.max(num2, num3));
    }
}