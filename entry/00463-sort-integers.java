// https://www.lintcode.com/problem/sort-integers/

public class Solution {
    /**
     * @param A: an integer array
     * @return: nothing
     */
    public void sortIntegers(int[] A) {
        // write your code here
        Arrays.sort(A);
    }
}