// https://www.lintcode.com/problem/longest-common-prefix/

public class Solution {
    /**
     * @param strs: A list of strings
     * @return: The longest common prefix
     */
    public String longestCommonPrefix(String[] strs) {
        // write your code here
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            char c = '\0';
            for (String str : strs) {
                if (i >= str.length()) {
                    c = '\0';
                    break;
                }
                if (c == '\0') {
                    c = str.charAt(i);
                } else {
                    if (str.charAt(i) != c) {
                        c = '\0';
                        break;
                    }
                }
            }
            if (c != '\0') {
                sb.append(c);
                ++i;
            } else {
                break;
            }
        }
        return sb.toString();
    }
}