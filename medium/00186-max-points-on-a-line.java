// https://www.lintcode.com/problem/max-points-on-a-line/

/**
 * Definition for a point.
 * class Point {
 *     int x;
 *     int y;
 *     Point() { x = 0; y = 0; }
 *     Point(int a, int b) { x = a; y = b; }
 * }
 */

public class Solution {
    /**
     * @param points: an array of point
     * @return: An integer
     */
    public int maxPoints(Point[] points) {
        // write your code here
        if (points != null) {
          int ret = 0;
          for (int i = 0; i < points.length; ++i) {
            Map<String, Integer> slopes = new HashMap<>();
            for (int j = 0; j < points.length; ++j) {
              if (i != j) {
                String slope = calcSlope(points[i], points[j]);
                if (!slopes.containsKey(slope)) {
                  slopes.put(slope, 2);
                } else {
                  slopes.put(slope, slopes.get(slope) + 1);
                }
              }
            }
            for (Integer v : slopes.values()) {
              if (v > ret) {
                ret = v;
              }
            }
          }
          return (ret > 0) ? ret : 1;
        }
        return 0;
    }

    protected String calcSlope(Point x, Point y) {
      int dy = y.y - x.y;
      int dx = y.x - x.x;
      if (dx == 0) {
        return "MAX";
      }
      if ((dx == 0) || (dy == 0)) {
        return "Zero";
      }
      boolean positive = ((dy * dx ) > 0);
      int a = Math.abs(dy);
      int b = Math.abs(dx);
      int gcd;
      while ((a % b) != 0) {
        int tmp = a % b;
        a = b;
        b = tmp;
      }
      gcd = b;
      String ret = String.valueOf(Math.abs(dy) / gcd) + "-" + String.valueOf(Math.abs(dx) / gcd);
      return positive ? ret : "-" + ret;
    }
}