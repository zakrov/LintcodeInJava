// https://www.lintcode.com/problem/linked-list-cycle/

/**
 * Definition for ListNode
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param head: The first node of linked list.
     * @return: True if it has a cycle, or false
     */
    public boolean hasCycle(ListNode head) {
        // write your code here
        ListNode one = head;
        ListNode two = head;
        while (two != null) {
            for (int i = 0; i < 2; ++i) {
                two = two.next;
                if (two != null) {
                    if (two == one) {
                        return true;
                    }
                } else {
                    break;
                }
            }
            one = one.next;
        }
        return false;
    }
}