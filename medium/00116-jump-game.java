// https://www.lintcode.com/problem/jump-game/

public class Solution {
    /**
     * @param A: A list of integers
     * @return: A boolean
     */
    public boolean canJump(int[] A) {
        // write your code here
        int target = A.length - 1;
        for (int i = A.length - 1; i >= 0; --i) {
            if ((i + A[i]) >= target) {
                target = i;
            }
        }
        return target == 0;
    }
}