// https://www.lintcode.com/problem/construct-binary-tree-from-inorder-and-postorder-traversal/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param inorder: A list of integers that inorder traversal of a tree
     * @param postorder: A list of integers that postorder traversal of a tree
     * @return: Root of a tree
     */
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        // write your code here
        return _buildTree(inorder, 0, inorder.length, postorder, 0, postorder.length);
    }
    
    private TreeNode _buildTree(int[] inorder, int istart, int iend, int[] postorder, int pstart, int pend) {
        if (istart >= iend) {
            return null;
        }
        int i = istart;
        while (i < iend) {
            if (inorder[i] == postorder[pend - 1]) {
                break;
            }
            ++i;
        }
        TreeNode root = new TreeNode(inorder[i]);
        int llen = i - istart;
        root.left = _buildTree(inorder, istart, i, postorder, pstart, pstart + llen);
        root.right = _buildTree(inorder, i + 1, iend, postorder, pstart + llen, pend - 1);
        return root;
    }
}