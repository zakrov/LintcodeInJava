// https://www.lintcode.com/problem/single-number-ii/

public class Solution {
    /**
     * @param A: An integer array
     * @return: An integer
     */
    public int singleNumberII(int[] A) {
        // write your code here
        int ret = 0;
        int tmp = 0;
        for (int a : A) {
            ret = (ret ^ a) & (~tmp);
            tmp = (tmp ^ a) & (~ret);
        }
        return ret;
    }
}