// https://www.lintcode.com/problem/anagrams/

public class Solution {
    /**
     * @param strs: A list of strings
     * @return: A list of strings
     */
    public List<String> anagrams(String[] strs) {
        // write your code here
        List<String> ret = new ArrayList<>();
        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] arr = str.toCharArray();
            Arrays.sort(arr);
            String sstr = String.valueOf(arr);
            if (!map.containsKey(sstr)) {
                map.put(sstr, new ArrayList<>());
            }
            map.get(sstr).add(str);
        }
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            List<String> words = entry.getValue();
            if (words.size() > 1) {
                for (String word : words) {
                    ret.add(word);
                }
            }
        }
        return ret;
    }
}