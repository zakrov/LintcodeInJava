// https://www.lintcode.com/problem/search-range-in-binary-search-tree/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param root: param root: The root of the binary search tree
     * @param k1: An integer
     * @param k2: An integer
     * @return: return: Return all keys that k1<=key<=k2 in ascending order
     */
    public List<Integer> searchRange(TreeNode root, int k1, int k2) {
        // write your code here
        List<Integer> ret = new ArrayList<>();
        if (root != null) {
            if ((root.val >= k1) && (root.val <= k2)) {
                ret.add(root.val);
            }
            if (root.val >= k1) {
                List<Integer> l = searchRange(root.left, k1, k2);
                for (Integer i : l) {
                    ret.add(i);
                }
            }
            if (root.val <= k2) {
                List<Integer> l = searchRange(root.right, k1, k2);
                for (Integer i : l) {
                    ret.add(i);
                }
            }
        }
        return ret;
    }
}