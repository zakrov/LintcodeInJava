// https://www.lintcode.com/problem/min-stack/

public class MinStack {
    protected Stack<Integer> stack;
    protected Stack<Integer> minStack;
    
    public MinStack() {
        // do intialization if necessary
        stack = new Stack<>();
        minStack = new Stack<>();
    }

    /*
     * @param number: An integer
     * @return: nothing
     */
    public void push(int number) {
        // write your code here
        stack.add(number);
        if ((minStack.size() > 0) && (number > minStack.peek())) { // 最小值不变
            minStack.add(minStack.peek());
        } else {
            minStack.add(number);
        }
    }

    /*
     * @return: An integer
     */
    public int pop() {
        // write your code here
        minStack.pop();
        return stack.pop();
    }

    /*
     * @return: An integer
     */
    public int min() {
        // write your code here
        return minStack.peek();
    }
}