// https://www.lintcode.com/problem/sort-letters-by-case/

public class Solution {
    /*
     * @param chars: The letter array you should sort by Case
     * @return: nothing
     */
    public void sortLetters(char[] chars) {
        // write your code here
        int i = 0;
        int clen = chars.length;
        int j = clen - 1;
        while (i < j) {
            while ((i < clen) && ((chars[i] >= 'a') && (chars[i] <= 'z'))) {
                ++i;
            }
            while ((j >= 0) && ((chars[j] >= 'A') && (chars[j] <= 'Z'))) {
                --j;
            }
            if (i < j) {
                char c = chars[i];
                chars[i] = chars[j];
                chars[j] = c;
                ++i;
                --j;
            }
        }
    }
}