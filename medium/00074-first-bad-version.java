// https://www.lintcode.com/problem/first-bad-version/

/**
 * public class SVNRepo {
 *     public static boolean isBadVersion(int k);
 * }
 * you can use SVNRepo.isBadVersion(k) to judge whether 
 * the kth code version is bad or not.
*/
public class Solution {
    /**
     * @param n: An integer
     * @return: An integer which is the first bad version.
     */
    public int findFirstBadVersion(int n) {
        // write your code here
        if (n <= 0) {
            return 0;
        }
        long start = 1, end = (long)n + 1;
        while (start < end) {
            int mid = (int)((start + end) / 2);
            System.out.println(mid);
            if (SVNRepo.isBadVersion(mid)) {
                end = mid;
            } else {
                start = mid + 1;
            }
        }
        return (int)start;
    }
}