// https://www.lintcode.com/problem/moving-circle/

public class Solution {
    /**
     * @param position: the position of circle A,B and point P.
     * @return: if two circle intersect return 1, otherwise -1.
     */
    public int IfIntersect(double[] position) {
        // 
        double xa = position[0];
        double ya = position[1];
        double ra = position[2];
        double xb = position[3];
        double yb = position[4];
        double rb = position[5];
        double xp = position[6];
        double yp = position[7];
        double dist_ab = Math.sqrt((xa - xb) * (xa - xb) + (ya - yb) * (ya - yb));
        double dist_bp = Math.sqrt((xp - xb) * (xp - xb) + (yp - yb) * (yp - yb));
        double min_rab = Math.abs(ra - rb);
        double max_rab = ra + rb;
        if ((min_rab <= dist_ab) && (dist_ab <= max_rab)) { // 初始相交
            return 1;
        }else if ((min_rab <= dist_bp) && (dist_bp <= max_rab)) { // P点相交
            return 1;
        }
        double a = yp - ya; // 这里公式都忘了...
        double b = xa - xp;
        double c = xp * ya - xa * yp;
        double dist = (a * xb + b * yb + c) / Math.sqrt(a * a + b * b);
        double xi = (b * b * xb - a * b * yb - a * c) / (a * a + b * b);
        double yi = (a * a * yb - a * b * xb - b * c) / (a * a + b * b);
        if ((min_rab <= dist) && (dist <= max_rab)) {
            if (isInline(new double[]{xa, ya},
                         new double[]{xp, yp},
                         new double[]{xi, yi},
                         ra)) { // 交点是不是在直线上
                return 1;
            }
        }
        return -1;
    }
    
    boolean isInline(double[] pa, double[] pp, double[] pb, double ra) {
        double max_x = pa[0] > pp[0] ? pa[0] : pp[0];
        double min_x = pa[0] > pp[0] ? pp[0] : pa[0];
        double max_y = pa[1] > pp[1] ? pa[1] : pp[1];
        double min_y = pa[1] > pp[1] ? pp[1] : pa[1];
        boolean flag = ((pb[0] - pa[0]) * (pp[1] - pa[1])) == ((pp[0] - pa[0]) * (pb[1] - pa[1]));
        return flag && ((min_x <= pb[0]) && (pb[0 ]<= max_x)) && ((min_y <= pb[1]) && (pb[1] <= max_y));
    }
}