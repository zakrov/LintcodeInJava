// https://www.lintcode.com/problem/largest-number/

public class Solution {
    /**
     * @param nums: A list of non negative integers
     * @return: A string
     */
    public String largestNumber(int[] nums) {
        // write your code here
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < nums.length; ++i) { // 如果是Integer类型就可以用Arrays.sort加自定义比较器
          for (int j = 0; j < (nums.length - 1); ++j) {
            int x = nums[j];
            int y = nums[j + 1];
            int x10 = log10(x);
            int y10 = log10(y);
            if ((x * Math.pow(10, y10) + y) < (y * Math.pow(10, x10) + x)) {
              int tmp = x;
              nums[j] = y;
              nums[j + 1] = tmp;
            }
          }
        }
        for (int n : nums) {
          if ((sb.length() > 0) || (n != 0)) {
            sb.append(String.valueOf(n));
          }
        }
        return (sb.length() > 0) ? sb.toString() : "0";
    }

    protected int log10(int n) {
      int ret = 0;
      while (n > 0) {
        n /= 10;
        ++ret;
      }
      return ret;
    }
}