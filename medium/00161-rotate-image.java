// https://www.lintcode.com/problem/rotate-image/

public class Solution {
    /**
     * @param matrix: a lists of integers
     * @return: nothing
     */
    public void rotate(int[][] matrix) {
        // write your code here
        int rows = matrix.length;
        if (rows > 1) {
          for (int i = 0; i < rows; ++i) {
            for (int j = (i + 1); j < rows; ++j) {
              int tmp = matrix[i][j];
              matrix[i][j] = matrix[j][i];
              matrix[j][i] = tmp;
            }
          }
          for (int i = 0; i < rows; ++i) {
            int start = 0;
            int end = matrix[i].length - 1;
            while (start < end) {
              int tmp = matrix[i][start];
              matrix[i][start] = matrix[i][end];
              matrix[i][end] = tmp;
              ++start;
              --end;
            }
          }
        }
    }
}