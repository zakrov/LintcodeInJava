// https://www.lintcode.com/problem/route-between-two-nodes-in-graph/

/**
 * Definition for Directed graph.
 * class DirectedGraphNode {
 *     int label;
 *     ArrayList<DirectedGraphNode> neighbors;
 *     DirectedGraphNode(int x) {
 *         label = x;
 *         neighbors = new ArrayList<DirectedGraphNode>();
 *     }
 * };
 */


public class Solution {
    /*
     * @param graph: A list of Directed graph node
     * @param s: the starting Directed graph node
     * @param t: the terminal Directed graph node
     * @return: a boolean value
     */
    public boolean hasRoute(ArrayList<DirectedGraphNode> graph, DirectedGraphNode s, DirectedGraphNode t) {
        // write your code here
        List<DirectedGraphNode> nodes = new ArrayList<>();
        Set<DirectedGraphNode> set = new HashSet<>();
        nodes.add(s);
        int pos = 0;
        while (pos < nodes.size()) {
            if (nodes.get(pos).equals(t)) {
                return true;
            }
            for (DirectedGraphNode node : nodes.get(pos).neighbors) {
                if (!set.contains(node)) {
                    set.add(node);
                    nodes.add(node);
                }
            }
            ++pos;
        }
        return false;
    }
}