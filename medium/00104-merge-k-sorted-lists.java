// https://www.lintcode.com/problem/merge-k-sorted-lists/

/**
 * Definition for ListNode.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int val) {
 *         this.val = val;
 *         this.next = null;
 *     }
 * }
 */ 
public class Solution {
    /**
     * @param lists: a list of ListNode
     * @return: The head of one sorted list.
     */
    public ListNode mergeKLists(List<ListNode> lists) {  
        // write your code here
        if (lists.isEmpty()) {
            return null;
        }
        List<ListNode> heap = new ArrayList<>();
        for (ListNode node : lists) {
            if (node != null) {
                heap.add(node);
            }
        }
        for (int i = heap.size() - 1; i > -1; --i) {
            adjustDown(heap, i);
        }
        ListNode head = null;
        ListNode tail = null;
        while (!heap.isEmpty()) {
            ListNode node = new ListNode(heap.get(0).val);
            if (head == null) {
                head = node;
            } else {
                tail.next = node;
            }
            tail = node;
            heap.set(0, heap.get(0).next);
            if (heap.get(0) == null) {
                heap.set(0, heap.get(heap.size() - 1));
                heap.remove(heap.size() - 1);
            }
            if (!heap.isEmpty()) {
                adjustDown(heap, 0);
            }
        }
        return head;
    }
    
    private void adjustDown(List<ListNode> heap, int i) {
        while (i < heap.size()) {
            int left = i * 2 + 1;
            int right = i * 2 + 2;
            int min_pos = i;
            if ((left < heap.size()) && (heap.get(left).val < heap.get(min_pos).val)) {
                min_pos = left;
            }
            if ((right < heap.size()) && (heap.get(right).val < heap.get(min_pos).val)) {
                min_pos = right;
            }
            if (min_pos != i) {
                ListNode tmp = heap.get(min_pos);
                heap.set(min_pos, heap.get(i));
                heap.set(i, tmp);
                i = min_pos;
            } else {
                break;
            }
        }
    }
}