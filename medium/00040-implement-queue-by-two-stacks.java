// https://www.lintcode.com/problem/implement-queue-by-two-stacks/

public class MyQueue {
    Stack<Integer> s1;
    Stack<Integer> s2;
    
    public MyQueue() {
        // do intialization if necessary
        s1 = new Stack<>();
        s2 = new Stack<>();
    }

    /*
     * @param element: An integer
     * @return: nothing
     */
    public void push(int element) {
        // write your code here
        while (s1.size() > 0) {
            s2.push(s1.pop());
        }
        s1.push(element);
        while (s2.size() > 0) {
            s1.push(s2.pop());
        }
    }

    /*
     * @return: An integer
     */
    public int pop() {
        // write your code here
        return s1.pop();
    }

    /*
     * @return: An integer
     */
    public int top() {
        // write your code here
        return s1.peek();
    }
}