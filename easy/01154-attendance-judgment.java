// https://www.lintcode.com/problem/attendance-judgment/

public class Solution {
    /**
     * @param record: Attendance record.
     * @return: If the student should be punished return true, else return false. 
     */
    public boolean judge(String record) {
        // Write your code here.
        int cd = 0, cl = 0;
        char pc = ' ';
        for (int i = 0; i < record.length(); ++i) {
            char c = record.charAt(i);
            if (c == 'D') {
                ++cd;
                if (cd == 2) {
                    return true;
                }
                pc = ' ';
                cl = 0;
            } else if (c == 'L') {
                if (pc != 'L') {
                    pc = 'L';
                    cl = 1;
                } else {
                    ++cl;
                    if (cl == 3) {
                        return true;
                    }
                }
            } else {
                pc = ' ';
                cl = 0;
            }
        }
        return false;
    }
}