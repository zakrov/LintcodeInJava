// https://www.lintcode.com/problem/moving-target/

public class Solution {
    /**
     * @param nums: a list of integer
     * @param target: an integer
     * @return: nothing
     */
    public void MoveTarget(int[] nums, int target) {
        // write your code here
        int steps = 0;
        for (int i = nums.length - 1; i > -1; --i) {
            if (nums[i] == target) {
                ++steps;
            } else {
                nums[i + steps] = nums[i];
            }
        }
        for (int i = 0; i < steps; ++i) {
            nums[i] = target;
        }
    }
}