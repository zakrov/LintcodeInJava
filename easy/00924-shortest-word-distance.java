// https://www.lintcode.com/problem/shortest-word-distance/

public class Solution {
    /**
     * @param words: a list of words
     * @param word1: a string
     * @param word2: a string
     * @return: the shortest distance between word1 and word2 in the list
     */
    public int shortestDistance(String[] words, String word1, String word2) {
        // Write your code here
        int ret = words.length;
        int i1 = -1, i2 = -1;
        for (int i = 0; i < words.length; ++i) {
            if (words[i].equals(word1)) {
                i1 = i;
            } else if (words[i].equals(word2)) {
                i2 = i;
            } else {
                continue;
            }
            if ((i1 >= 0) && (i2 >= 0)) {
                ret = Math.min(ret, Math.abs(i1 - i2));
            }
        }
        return ret;
    }
}