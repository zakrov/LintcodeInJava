// https://www.lintcode.com/problem/detect-capital/

public class Solution {
    /**
     * @param word: a string
     * @return: return a boolean
     */
    public boolean detectCapitalUse(String word) {
        // write your code here
        if (word.charAt(0) == Character.toUpperCase(word.charAt(0))) {
            String s = word.substring(1);
            return (s.equals(s.toUpperCase())) || (s.equals(s.toLowerCase()));
        } else {
            String s = word.substring(1);
            return s.equals(s.toLowerCase());
        }
    }
}