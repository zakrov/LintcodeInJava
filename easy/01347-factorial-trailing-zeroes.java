// https://www.lintcode.com/problem/factorial-trailing-zeroes/

public class Solution {
    /**
     * @param n: a integer
     * @return: return a integer
     */
    public int trailingZeroes(int n) {
        // write your code here
        int ret = 0;
        while (n > 0) {
            ret += (n / 5);
            n /= 5;
        }
        return ret;
    }
}