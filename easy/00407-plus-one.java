// https://www.lintcode.com/problem/plus-one/

public class Solution {
    /**
     * @param digits: a number represented as an array of digits
     * @return: the result
     */
    public int[] plusOne(int[] digits) {
        // write your code here
        digits[digits.length - 1] += 1;
        int step = (digits[digits.length - 1] >= 10) ? 1 : 0;
        digits[digits.length - 1] %= 10;
        for (int  i = digits.length - 2; i >= 0; --i) {
            digits[i] += step;
            step = (digits[i] >= 10) ? 1 : 0;
            digits[i] %= 10;
        }
        if (step > 0) {
            int[] ret = new int[digits.length + 1];
            ret[0] = step;
            for (int i = 0; i < digits.length; ++i) {
                ret[i + 1] = digits[i];
            }
            return ret;
        }
        else {
            return digits;            
        }
    }
}