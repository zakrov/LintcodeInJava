// https://www.lintcode.com/problem/sort-integers-ii/

public class Solution {
    /**
     * @param A: an integer array
     * @return: nothing
     */
    public void sortIntegers2(int[] A) {
        // write your code here
        qsort(A, 0, A.length - 1);
    }
    
    public void qsort(int[] A, int start, int end) {
        if (start < end) {
            int p = partition(A, start, end);
            qsort(A, start, p - 1);
            qsort(A, p + 1, end);
        }
    }

    public int partition(int[] A, int start, int end) {
        int pivot = A[end];
        int i = start;
        for (int j = start; j < end; j++) {
            if (A[j] < pivot) {
                int t = A[i];
                A[i] = A[j];
                A[j] = t;
                i++;
            }
        }
        int t = A[i];
        A[i] = A[end];
        A[end] = t;
        return i;
    }
}