// https://www.lintcode.com/problem/circle/

public class Solution {
    /**
     * @param circles: The value of 6 points on n rings
     * @return: Whether there are two same circles
     */
    public boolean samecircle(int[][] circles) {
        // write your code here
        Set<String> s = new HashSet<>();
        for (int i = 0; i < circles.length; ++i) {
            int[] circle = circles[i];
            Arrays.sort(circle);
            StringBuilder sb = new StringBuilder();
            for (int c : circle) {
                sb.append(Integer.toString(c));
                sb.append('-');
            }
            String _circle = sb.toString();
            if (s.contains(_circle)) {
                return true;
            } else {
                s.add(_circle);
            }
        }
        return false;
    }
}