// https://www.lintcode.com/problem/time-angle/

public class Solution {
    /**
     * @param h: hours
     * @param m: minutes
     * @return: angle between hour hand and minute hand at X:Y in a clock
     */
    public float timeAngle(int h, int m) {
        // write your code here
        float fh = (float)h * 5.0f + (float)m / 12.0f;
        float ret = Math.abs((fh - (float)m) * 6.0f);
        return (ret > 180) ? (360 - ret) : ret;
    }
}