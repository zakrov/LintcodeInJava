// https://www.lintcode.com/problem/calculate-circumference-and-area/

public class Solution {
    /**
     * @param r: a Integer represent radius
     * @return: the circle's circumference nums[0] and area nums[1]
     */
    public double[] calculate(int r) {
        // write your code here
        double pi = 3.14; // 因为r整数，所以pi两位精度够了。
        return new double[]{pi * 2.0 * r, pi * r * r};
    }
}