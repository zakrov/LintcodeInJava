// https://www.lintcode.com/problem/happy-number/

public class Solution {
    /**
     * @param n: An integer
     * @return: true if this is a happy number or false
     */
    public boolean isHappy(int n) {
        // write your code here
        set = new HashSet<>();
        while (true) {
            int s = 0;
            while (n > 0) {
                s += (n % 10) * (n % 10);
                n /= 10;
            }
            if (1 == s) {
                return true;
            }
            if (set.contains(s)) {
                return false;
            }
            set.add(s);
            n = s;
        }
    }
    
    private HashSet<Integer> set;
}