// https://www.lintcode.com/problem/valid-anagram/

public class Solution {
    /**
     * @param s: The first string
     * @param t: The second string
     * @return: true or false
     */
    public boolean anagram(String s, String t) {
        // write your code here
        if (s.length() != t.length()) {
            return false;
        }
        int[] chars = new int[256];
        for (int i = 0; i < 256; ++i) {
            chars[i] = 0;
        }
        for (int i = 0; i < s.length(); ++i) {
            chars[Integer.valueOf(s.charAt(i))] += 1;
            chars[Integer.valueOf(t.charAt(i))] -= 1;
        }
        for (int i = 0; i < 256; ++i) {
            if (chars[i] != 0) {
                return false;
            }
        }
        return true;
    }
}