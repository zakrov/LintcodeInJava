// https://www.lintcode.com/problem/set-mismatch/

public class Solution {
    /**
     * @param nums: an array
     * @return: the number occurs twice and the number that is missing
     */
    public int[] findErrorNums(int[] nums) {
        // Write your code here
        int[] ret = new int[2];
        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; ++i) {
            if (!set.contains(nums[i])) {
                set.add(nums[i]);
            } else {
                ret[0] = nums[i]; 
            }
        }
        for (int i = 1; i < nums.length + 1; ++i) {
            if (!set.contains(i)) {
                ret[1] = i;
                break;
            }
        }
        return ret;
    }
}