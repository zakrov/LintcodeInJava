// https://www.lintcode.com/problem/largest-letter/

public class Solution {
    /**
     * @param s: a string
     * @return: a string
     */
    public String largestLetter(String s) {
        // write your code here
        String ret = "NO";
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < s.length(); ++i) {
            Character c = s.charAt(i);
            set.add(c);
        }
        for (int i = 0; i < s.length(); ++i) {
            Character c = s.charAt(i);
            Character uc = Character.toUpperCase(c);
            Character lc = Character.toLowerCase(c);
            if (set.contains(uc) && set.contains(lc)) {
                if (ret.equals("NO")) {
                    ret = Character.toString(uc);
                } else {
                    String ucs = Character.toString(uc);
                    if (ucs.compareTo(ret) > 0) {
                        ret = ucs;
                    }
                }
            }
        }
        return ret;
    }
}