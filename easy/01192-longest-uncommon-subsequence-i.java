// https://www.lintcode.com/problem/longest-uncommon-subsequence-i/

public class Solution {
    /**
     * @param a: a string
     * @param b: a string
     * @return: return a integer
     */
    public int findLUSlength(String a, String b) {
        // write your code here
        String s1 = a.replace(b, "");
        String s2 = b.replace(a, "");
        int ret = Math.max(s1.length(), s2.length());
        return (ret > 0) ? ret : -1;
    }
}