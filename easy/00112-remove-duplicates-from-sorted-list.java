// https://www.lintcode.com/problem/remove-duplicates-from-sorted-list/

/**
 * Definition for ListNode
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param head: head is the head of the linked list
     * @return: head of linked list
     */
    public ListNode deleteDuplicates(ListNode head) {
        // write your code here
        ListNode new_head = null, new_tail = null;
        while (head != null) {
            if (null == new_head) {
                new_head = head;
                new_tail = head;
                head = head.next;
                new_tail.next = null;
            }
            else {
                if (head.val != new_tail.val) {
                    new_tail.next = head;
                    new_tail = new_tail.next;
                    head = head.next;
                    new_tail.next = null;
                }
                else {
                    head = head.next;
                }
            }
        }
        return new_head;
    }
}