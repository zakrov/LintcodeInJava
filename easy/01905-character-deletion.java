// https://www.lintcode.com/problem/character-deletion/

public class Solution {
    /**
     * @param str: The first string given
     * @param sub: The given second string
     * @return: Returns the deleted string
     */
    public String CharacterDeletion(String str, String sub) {
        // write your code here
        StringBuilder ret = new StringBuilder();
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < sub.length(); ++i) {
            set.add(sub.charAt(i));
        }
        for (int i = 0; i < str.length(); ++i) {
            Character c = str.charAt(i);
            if (!set.contains(c)) {
                ret.append(c);
            }
        }
        return ret.toString();
    }
}