// https://www.lintcode.com/problem/1902/

public class Solution {
    /**
     * @param S: The c++ file
     * @return: return if there is "Google" in commet line
     */
    public boolean FindGoogle(List<String> S) {
        // Write your code here.
        boolean lc = false;
        boolean mlc = false;
        String google = "";
        for (String s : S) {
          char prev = '\0';
          for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if ((prev == '/') && (c == '/') && (!mlc)) {
              lc = true;
            } else if ((prev == '/') && (c == '*') && (!lc)) {
              mlc = true;
            } else if ((prev == '*') && (c == '/') && mlc) {
              mlc = false;
            } else if (lc || mlc) {
              if (c == 'G') {
                google = "G";
              } else if ((c == 'o') && (google == "G")) {
                google = "Go";
              } else if ((c == 'o') && (google == "Go")) {
                google = "Goo";
              } else if ((c == 'g') && (google == "Goo")) {
                google = "Goog";
              } else if ((c == 'l') && (google == "Goog")) {
                google = "Googl";
              } else if ((c == 'e') && (google == "Googl")) {
                return true;
              } else {
                google = "";
              }
            } else {
              google = "";
            }
            prev = c;
          }
          lc = false;
        }
        return false;
    }
}