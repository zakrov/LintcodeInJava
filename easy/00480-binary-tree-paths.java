// https://www.lintcode.com/problem/binary-tree-paths/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param root: the root of the binary tree
     * @return: all root-to-leaf paths
     */
    public List<String> binaryTreePaths(TreeNode root) {
        // write your code here
        ret = new ArrayList<>();
        _binaryTreePaths(root, "");
        return ret;
    }
    
    private void _binaryTreePaths(TreeNode root, String path) {
        if (root != null) {
            if (0 == path.length()) {
                path += Integer.toString(root.val);
            }
            else {
                path += "->" + Integer.toString(root.val);
            }
            if (root.left != null) {
                _binaryTreePaths(root.left, path);
            }
            if (root.right != null) {
                _binaryTreePaths(root.right, path);
            }
            if ((null == root.left) && (null == root.right)) {
                ret.add(path);
            }
        }
    }
    
    private List<String> ret;
}