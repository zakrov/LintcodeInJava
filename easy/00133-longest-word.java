// https://www.lintcode.com/problem/longest-word/

public class Solution {
    /*
     * @param dictionary: an array of strings
     * @return: an arraylist of strings
     */
    public List<String> longestWords(String[] dictionary) {
        // write your code here
        int max_len = 0;
        HashMap<Integer, ArrayList<String>> map = new HashMap<>();
        for (String s : dictionary) {
            int len = s.length();
            if (len > max_len) {
                max_len = len;
            }
            if (!map.containsKey(len)) {
                map.put(len, new ArrayList<String>());
            }
            map.get(len).add(s);
        }
        return map.get(max_len);
    }
}