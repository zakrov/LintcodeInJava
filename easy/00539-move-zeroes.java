// https://www.lintcode.com/problem/move-zeroes/

public class Solution {
    /**
     * @param nums: an integer array
     * @return: nothing
     */
    public void moveZeroes(int[] nums) {
        // write your code here
        int steps = 0, zeroes = 0;
        for (int i = 0; i < nums.length; ++i) {
            if (0 == nums[i]) {
                steps += 1;
                zeroes += 1;
            } else {
                nums[i - steps] = nums[i];
            }
        }
        for (int i = 0; i < zeroes; ++i) {
            nums[nums.length - 1 - i] = 0;
        }
    }
}