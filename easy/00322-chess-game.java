// https://www.lintcode.com/problem/chess-game/

public class Solution {
    /**
     * @param queen: queen‘coordinate
     * @param knight: knight‘coordinate
     * @return: if knight is attacked please return true，else return false
     */
    public boolean[] isAttacked(int[][] queen, int[][] knight) {
        // write your code here
        boolean[] ret = new boolean[knight.length];
        Set<Integer> xSet = new HashSet<>();
        Set<Integer> ySet = new HashSet<>();
        Set<Integer> diffPositiveSet = new HashSet<>(); // 斜率为1
        Set<Integer> diffNegativeSet = new HashSet<>(); // 斜率为-1
        for (int[] q : queen) {
            xSet.add(q[0]);
            ySet.add(q[1]);
            diffPositiveSet.add(q[1] - q[0]);
            diffNegativeSet.add(q[0] + q[1]);
        }
        for (int i = 0; i < knight.length; ++i) {
            int[] k = knight[i];
            if (xSet.contains(k[0])) {
                ret[i] = true;
                continue;
            }
            if (ySet.contains(k[1])) {
                ret[i] = true;
                continue;
            }
            if ((diffPositiveSet.contains(k[1] - k[0])) || (diffNegativeSet.contains(k[0] + k[1]))) {
                ret[i] = true;
                continue;
            }
            ret[i] = false;
        }
        return ret;
    }
}