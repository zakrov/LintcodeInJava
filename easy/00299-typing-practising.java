// https://www.lintcode.com/problem/typing-practising/

public class Solution {
    /**
     * @param s: A string
     * @return: A string
     */
    public String getTextcontent(String s) {
        // write your code here.
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            Character c = s.charAt(i);
            if (c != '<') {
                sb.append(c);
            } else {
                if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
            }
        }
        return sb.toString();
    }
}