-- https://www.lintcode.com/problem/find-a-classmate-with-the-same-name/

select name from students  group by name having count(name) > 1