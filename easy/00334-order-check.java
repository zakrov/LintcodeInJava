// https://www.lintcode.com/problem/order-check/

public class Solution {
    /**
     * @param heights: Students height
     * @return: How many people are not where they should be
     */
    public int orderCheck(List<Integer> heights) {
        // write your code here
        int ret = 0;
        List<Integer> copied = new ArrayList<>(heights);
        Collections.sort(copied);
        for (int i = 0; i < heights.size(); ++i) {
            if (heights.get(i)!= copied.get(i)) {
                ++ret;
            }
        }
        return ret;
    }
}