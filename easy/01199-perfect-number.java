// https://www.lintcode.com/problem/perfect-number/

public class Solution {
    /**
     * @param num: an integer
     * @return: returns true when it is a perfect number and false when it is not
     */
    public boolean checkPerfectNumber(int num) {
        // write your code here
        if (num == 1) {
            return false;
        }
        int ret = 1, i = 2;
        while ((i * i) <= num) {
            if ((num % i) == 0) {
                ret += (i + (num / i));
            }
            ++i;
        }
        return ret == num;
    }
}