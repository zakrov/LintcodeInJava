// https://www.lintcode.com/problem/reverse-order-storage/

/**
 * Definition for ListNode
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param head: the given linked list
     * @return: the array that store the values in reverse order 
     */
    public List<Integer> reverseStore(ListNode head) {
        // write your code here
        List<Integer> ret = new ArrayList<>();
        while (head != null) {
            ret.add(head.val);
            head = head.next;
        }
        Collections.reverse(ret);
        return ret;
    }
}