// https://www.lintcode.com/problem/implement-strstr/

public class Solution {
    /**
     * @param source: 
     * @param target: 
     * @return: return the index
     */
    public int strStr(String source, String target) {
        // Write your code here
        int l_s = source.length(), l_t = target.length();
        if (l_s < l_t) {
            return -1;
        }
        else if (l_t == 0) {
            return 0;
        }
        for (int i = 0; i <= (l_s - l_t); ++i) {
            if (source.substring(i, i + l_t).equals(target)) {
                return i;
            }
        }
        return -1;
        
    }
}