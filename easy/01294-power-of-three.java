// https://www.lintcode.com/problem/power-of-three/

public class Solution {
    /**
     * @param n: an integer
     * @return: if n is a power of three
     */
    public boolean isPowerOfThree(int n) {
        // Write your code here
        // 用log的方法我不认为合理，只是隐藏了循环。
        boolean ret = false;
        int i = 1;
        while (i <= n) {
            if (i == n) {
                ret = true;
                break;
            }
            i *= 3;
        }
        return ret;
    }
}