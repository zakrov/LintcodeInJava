// https://www.lintcode.com/problem/flip-game/

public class Solution {
    /**
     * @param s: the given string
     * @return: all the possible states of the string after one valid move
     */
    public List<String> generatePossibleNextMoves(String s) {
        // write your code here
        List<String> ret = new ArrayList<>();
        for (int i = 0; i < s.length() - 1; ++i) {
            StringBuilder sb = new StringBuilder();
            if ((s.charAt(i) == '+') && (s.charAt(i + 1) == '+')) {
                if (i > 0) {
                    sb.append(s.substring(0, i));
                }
                sb.append("--");
                if ((i + 2) < s.length()) {
                    sb.append(s.substring(i + 2));
                }
            }
            if (sb.length() > 0) {
                ret.add(sb.toString());
            }
        }
        return ret;
    }
}