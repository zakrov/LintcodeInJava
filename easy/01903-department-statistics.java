// https://www.lintcode.com/problem/department-statistics/

public class Solution {
    /**
     * @param employees: information of the employees
     * @param friendships: the friendships of employees
     * @return: return the statistics
     */
    public List<String> departmentStatistics(List<String> employees, List<String> friendships) {
        // write your code here.
        List<String> ret = new ArrayList<>();
        Map<String, Integer> mapCounting = new HashMap<>();
        Map<String, Set<String>> mapFriends = new HashMap<>();
        Map<String, String> mapBu = new HashMap<>();
        for (String e : employees) {
          String[] ss = e.split(", ");
          String id = ss[0];
          String bu = ss[2];
          mapBu.put(id, bu);
          if (!mapCounting.containsKey(bu)) {
            mapCounting.put(bu, 0);
          }
          mapCounting.put(bu, mapCounting.get(bu) + 1);
          if (!mapFriends.containsKey(bu)) {
            mapFriends.put(bu, new HashSet<>());
          }
        }
        for (String f : friendships) {
          String[] ids = f.split(", ");
          String bu0 = mapBu.get(ids[0]);
          String bu1 = mapBu.get(ids[1]);
          if (bu0.compareTo(bu1) != 0) { // 不能用==比较
            mapFriends.get(bu0).add(ids[0]);
            mapFriends.get(bu1).add(ids[1]);
          }
        }
        for (String bu : mapCounting.keySet()) {
          StringBuilder sb = new StringBuilder();
          sb.append(bu);
          sb.append(": ");
          sb.append(mapFriends.get(bu).size());
          sb.append(" of ");
          sb.append(mapCounting.get(bu));
          ret.add(sb.toString());
        }
        return ret;
    }
}