// https://www.lintcode.com/problem/add-and-search/

public class Solution {
    /**
     * @param inputs: an integer array
     * @param tests: an integer array
     * @return: return true if sum of two values in inputs are in tests.
     */
    public boolean addAndSearch(int[] inputs, int[] tests) {
        // write your code here.
        Set<Integer> set = new HashSet<>();
        for (int i : tests) {
            set.add(i);
        }
        for (int i = 0; i < inputs.length - 1; ++i) {
            for (int j = i + 1; j < inputs.length; ++j) {
                int s = inputs[i] + inputs[j];
                if (set.contains(s)) {
                    return true;
                }
            }
        }
        return false;
    }
}