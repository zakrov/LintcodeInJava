// https://www.lintcode.com/problem/swap-nodes-in-pairs/

/**
 * Definition for ListNode
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param head: a ListNode
     * @return: a ListNode
     */
    public ListNode swapPairs(ListNode head) {
        // write your code here
        ListNode dummy_head = new ListNode(0); // 简化head处理
        dummy_head.next = head;
        ListNode prev = dummy_head, node = head;
        while (node != null) {
            ListNode next = node.next;
            if (null == next) {
                break;
            }
            ListNode next_next = next.next;
            prev.next = next;
            next.next = node;
            node.next = next_next;
            prev = node;
            node = node.next;
        }
        return dummy_head.next;
    }
}