// https://www.lintcode.com/problem/find-elements-in-matrix/

public class Solution {
    /**
     * @param Matrix: the input
     * @return: the element which appears every row
     */
    public int FindElements(int[][] Matrix) {
        // write your code here
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < Matrix.length; ++i) {
            HashMap<Integer, Integer> row_map = new HashMap<>();
            for (int j = 0; j < Matrix[0].length; ++j) {
                row_map.put(Matrix[i][j], 1);
            }
            for (Map.Entry<Integer, Integer> entry : row_map.entrySet()) {
                int key = entry.getKey();
                if (!map.containsKey(key)) {
                    map.put(key, 0);
                }
                map.put(key, map.get(key) + 1);
            }
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == Matrix.length) {
                return entry.getKey();
            }
        }
        return 0; // 不会到这里，假设一定有答案。
    }
}