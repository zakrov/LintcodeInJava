// https://www.lintcode.com/problem/two-sum/

public class Solution {
    /**
     * @param numbers: An array of Integer
     * @param target: target = numbers[index1] + numbers[index2]
     * @return: [index1, index2] (index1 < index2)
     */
    public int[] twoSum(int[] numbers, int target) {
        // write your code here
        HashMap<Integer, Integer> map = new HashMap<>(); // 查表最快
        for (int i = 0; i < numbers.length; ++i) {
            map.put(numbers[i], i);
        }
        for (int i = 0; i < numbers.length; ++i) {
            int j = target - numbers[i];
            if (map.containsKey(j)) {
                int k = map.get(j);
                return new int[]{Math.min(i, k), Math.max(i, k)};
            }
        }
        return new int[]{-1, -1}; // Dummy return
    }
}