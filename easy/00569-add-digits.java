// https://www.lintcode.com/problem/add-digits/

public class Solution {
    /**
     * @param num: a non-negative integer
     * @return: one digit
     */
    public int addDigits(int num) {
        // write your code here
        int ret = 0;
        while (num > 0) {
            ret += (num % 10);
            num /= 10;
        }
        if (ret >= 10) {
            ret = addDigits(ret);
        }
        return ret;
    }
}