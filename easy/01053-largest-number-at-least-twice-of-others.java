// https://www.lintcode.com/problem/largest-number-at-least-twice-of-others/

public class Solution {
    /**
     * @param nums: a integer array
     * @return: the index of the largest element
     */
    public int dominantIndex(int[] nums) {
        // Write your code here
        if (nums.length < 2) {
            return 0;
        }
        int v1 = nums[0], v2 = nums[1];
        int p1 = 0, p2 = 1;
        if (v1 < v2) {
            int tmp = v1; // Java恶心！！！
            v1 = v2;
            v2 = tmp;
            tmp = p1;
            p1 = p2;
            p2 = tmp;
        }
        for (int i = 2; i < nums.length; ++i) {
            if (nums[i] > v2) {
                v2 = nums[i];
                p2 = i;
                if (v1 < v2) {
                    int tmp = v1;
                    v1 = v2;
                    v2 = tmp;
                    tmp = p1;
                    p1 = p2;
                    p2 = tmp;
                }
            }
        }
        return ((v2 * 2) <= v1) ? p1 : -1;
    }
}