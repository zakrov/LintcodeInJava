// https://www.lintcode.com/problem/container-design/

public class MyContainer {
    /**
     * @param element: Add element into this container.
     * @return: nothing
     */
    public void add(int element) {
        // write your code here.
        sum += element;
        list.add(element);
    }

    /**
     * @return: Sum of integers.
     */
    public int getSum() {
        // write your code here.
        return sum;
    }
    
    MyContainer() {
        list = new ArrayList<>();
        sum = 0;
    }
    
    protected List<Integer> list;
    protected int sum;
}