// https://www.lintcode.com/problem/longest-continuous-increasing-subsequence/

public class Solution {
    /**
     * @param A: An array of Integer
     * @return: an integer
     */
    public int longestIncreasingContinuousSubsequence(int[] A) {
        // write your code here
        if (A.length <= 1) {
            return A.length;
        }
        int dir = (A[0] < A[1]) ? 1 : -1; // 上升 or 下降
        int i = 1, seq_count = 1, max_seq_count = 0;
        while (i < A.length) {
            if (dir > 0) {
                if (A[i] > A[i - 1]) {
                    ++seq_count;
                }
                else {
                    if (seq_count > max_seq_count) {
                        max_seq_count = seq_count;
                    }
                    seq_count = 2;
                    dir = -1;
                }
            }
            else {
                if (A[i] < A[i - 1]) {
                    ++seq_count;
                }
                else {
                    if (seq_count > max_seq_count) {
                        max_seq_count = seq_count;
                    }
                    seq_count = 2;
                    dir = 1;
                }
                
            }
            ++i;
        }
        return (max_seq_count > seq_count) ? max_seq_count : seq_count;
    }
}