// https://www.lintcode.com/problem/intersection-of-two-arrays/

public class Solution {
    /**
     * @param nums1: an integer array
     * @param nums2: an integer array
     * @return: an integer array
     */
    public int[] intersection(int[] nums1, int[] nums2) {
        // write your code here
        List<Integer> result = new ArrayList<>();
        HashMap<Integer, Integer> map = new HashMap<>();
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        for (int i = 0; i < nums1.length; ++i) {
            if (!map.containsKey(nums1[i])) {
                map.put(nums1[i], 1);
            }
        }
        for (int i = 0; i < nums2.length; ++i) {
            if ((map.containsKey(nums2[i])) && (map.get(nums2[i]) > 0)) {
                result.add(nums2[i]);
                map.put(nums2[i], 0);
            }
        }
        return result.stream().mapToInt(i -> i).toArray();
    }
}