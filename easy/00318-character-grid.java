// https://www.lintcode.com/problem/character-grid/

public class Solution {
    /**
     * @param A: A string
     * @param B: A string
     * @return: A string array
     */
    public List<String> characterGrid(String A, String B) {
        // write your code here.
        int bLen = B.length();
        int aLen = A.length();
        List<String> ret = new ArrayList<>();
        Map<Character, Integer> mapA = initMap(A);
        Map<Character, Integer> mapB = initMap(B);
        int row = 0;
        int col = 0;
        for (int i = 0; i < A.length(); ++i) {
            Character c = A.charAt(i);
            if (mapB.containsKey(c)) {
                col = mapA.get(c);
                row = mapB.get(c);
                break;
            }
        }
        for (int i = 0; i < bLen; ++i) {
            if (i != row) {
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < aLen; ++j) {
                    if (j != col) {
                        sb.append('.');
                    } else {
                        sb.append(B.charAt(i));
                    }
                }
                ret.add(sb.toString());
            } else {
                ret.add(A);
            }
        }
        return ret;
    }
    
    private Map<Character, Integer> initMap(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); ++i) {
            Character c = s.charAt(i);
            if (!map.containsKey(c)) {
                map.put(c, i);
            }
        }
        return map;
    }
}