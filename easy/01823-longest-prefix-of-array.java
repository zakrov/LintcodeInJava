// https://www.lintcode.com/problem/longest-prefix-of-array/

public class Solution {
    /**
     * @param X: a integer
     * @param Y: a integer
     * @param nums: a list of integer
     * @return: return the maximum index of largest prefix
     */
    public int LongestPrefix(int X, int Y, int[] nums) {
        // write your code here
        int ret = -1;
        int cx = 0, cy = 0;
        for (int i = 0; i < nums.length; ++i) {
            int v = nums[i];
            if (v == X) {
                ++cx;
            } else if (v == Y) {
                ++cy;
            }
            if ((cx > 0) && (cx == cy)) {
                ret = i;
            }
        }
        return ret;
    }
}