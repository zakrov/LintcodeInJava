// https://www.lintcode.com/problem/implement-queue-by-linked-list/

public class MyQueue {
    ListNode head, tail;
    
    MyQueue() {
        head = null;
        tail = null;
    }
    /*
     * @param item: An integer
     * @return: nothing
     */
    public void enqueue(int item) {
        // write your code here
        ListNode node = new ListNode(item);
        if (head == null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            tail = tail.next;
        }
    }

    /*
     * @return: An integer
     */
    public int dequeue() {
        // write your code here
        int ret = head.val;
        head = head.next;
        return ret;
    }
}