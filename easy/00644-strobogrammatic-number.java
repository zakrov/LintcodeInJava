// https://www.lintcode.com/problem/strobogrammatic-number/

public class Solution {
    /**
     * @param num: a string
     * @return: true if a number is strobogrammatic or false
     */
    public boolean isStrobogrammatic(String num) {
        // write your code here
        int i = 0, j = num.length() - 1;
        while (i <= j) {
            char ci = num.charAt(i), cj = num.charAt(j);
            boolean cond = ((ci == '6') && (cj == '9'));
            cond = (cond || ((ci == '9') && (cj == '6')));
            cond = (cond || ((ci == '0') && (cj == '0')));
            cond = (cond || ((ci == '1') && (cj == '1')));
            cond = (cond || ((ci == '8') && (cj == '8')));
            if (!cond) {
                return false;
            }
            ++i;
            --j;
        }
        return true;
    }
}