// https://www.lintcode.com/problem/group-buy/

public class Solution {
    /**
     * @param x: the number of people who plan to buy goods A.
     * @param y: the number of people who plan to buy goods B.
     * @param z: the number of people who plan to buy goods C.
     * @return: return the maximum times they can group buy.
     */
    public int groupBuyTimes(int x, int y, int z) {
        // write your code here
        if (z >= x) {
            return (x >= y) ? y : x;
        }
        if (z >= y) {
            return (x >= y) ? y : x;
        }
        x -= z;
        y -= z;
        int r = (x + y) / 3;
        if (r > Math.min(x, y)) {
            r = Math.min(x, y);
        }
        return r + z;
    }
}