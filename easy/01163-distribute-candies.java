// https://www.lintcode.com/problem/distribute-candies/

public class Solution {
    /**
     * @param candies: a list of integers
     * @return: return a integer
     */
    public int distributeCandies(int[] candies) {
        // write your code here
        // 理想状态[1, 2, 3], [1, 2, 3]
        // 如果不平均的话，糖果数量的上限是1/2，下限是糖果的种类。
        HashSet<Integer> set = new HashSet<>();
        for (int c : candies) {
            set.add(c);
        }
        return Math.min(set.size(), candies.length / 2);
    }
}