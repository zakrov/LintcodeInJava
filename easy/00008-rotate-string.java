// https://www.lintcode.com/problem/rotate-string/

public class Solution {
    /**
     * @param str: An array of char
     * @param offset: An integer
     * @return: nothing
     */
    public void rotateString(char[] str, int offset) {
        // write your code here
        if (str.length == 0) {
            return;
        }
        offset %= str.length;
        reverse(str, 0, str.length - 1);
        reverse(str, 0, offset - 1);
        reverse(str, offset, str.length - 1);
    }
    
    private void reverse(char[] str, int start, int end) {
        while (start < end) {
            char c = str[start];
            str[start] = str[end];
            str[end] = c;
            ++start;
            --end;
        }
    }
}