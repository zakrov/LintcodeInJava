// https://www.lintcode.com/problem/trailing-zeros/

public class Solution {
    /*
     * @param n: An integer
     * @return: An integer, denote the number of trailing zeros in n!
     */
    public long trailingZeros(long n) {
        // write your code here, try to do it without arithmetic operators.
        int ret = 0;
        while (n > 0) {
            // 2 * 5 = 10，而且2总比5。
            n = n / 5;
            ret += n;
        }
        return ret;
    }
}