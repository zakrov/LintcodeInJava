// https://www.lintcode.com/problem/maximum-non-negative-subarray/

public class Solution {
    /**
     * @param A: an integer array
     * @return: return maxium contiguous non-negative subarray sum
     */
    public int maxNonNegativeSubArray(int[] A) {
        // write your code here
        int ret = -1;
        int s = 0;
        for (int i : A) {
            if (i >= 0) {
                s += i;
                if (s > ret) {
                    ret = s;
                }
            } else {
                s = 0;
            }
        }
        return ret;
    }
}