// https://www.lintcode.com/problem/the-longest-common-prefix-ii/

public class Solution {
    /**
     * @param dic: the n strings
     * @param target: the target string
     * @return: The ans
     */
    public int the_longest_common_prefix(List<String> dic, String target) {
        // write your code here
        int ret = 0;
        for (int i = 0; i < dic.size(); ++i) {
            int tmp = 0;
            String word = dic.get(i);
            for (int j = 0; j < Math.min(word.length(), target.length()); ++j) {
                if (word.charAt(j) == target.charAt(j)) {
                    ++tmp;
                } else {
                    break;
                }
            }
            if (tmp > ret) {
                ret = tmp;
            }
        }
        return ret;
    }
}