// https://www.lintcode.com/problem/slide-soduku/

public class Solution {
    /**
     * @param number: an only contains from 1 to 9 array
     * @return: return  whether or not each sliding window position contains all the numbers for 1 to 9 
     */
    public boolean[] SlidingWindows(int[][] number) {
        // write your code here
        int n = number[0].length;
        boolean[] ret = new boolean[n - 2];
        Map<Integer, Integer> map = new HashMap<>();
        for (int start = 0; start < (n - 2); ++start) {
            for (int r = 0; r < 3; ++r) {
                for (int c = 0; c < 3; ++c) {
                    int v = number[r][c + start];
                    if (!map.containsKey(v)) {
                        map.put(v, 0);
                    }
                    map.put(v, map.get(v) + 1);
                }
            }
            ret[start] = check(map);
            clear(map);
        }
        return ret;
    }
    
    private boolean check(Map<Integer, Integer> map) {
        if (map.size() != 9) {
            return false;
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() != 1) {
                return false;
            }
        }
        return true;
    }
    
    private void clear(Map<Integer, Integer> map) {
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            entry.setValue(0);
        }
    }
}