// https://www.lintcode.com/problem/first-unique-character-in-a-string/

public class Solution {
    /**
     * @param str: str: the given string
     * @return: char: the first unique character in a given string
     */
    public char firstUniqChar(String str) {
        // Write your code here
        int[] mapping = new int[58]; // 查表大法好，a-z ... A-Z
        for (int i = 0; i < 58; ++i) {
            mapping[i] = 0;
        }
        for (int i = 0; i < str.length(); ++i) {
            mapping[(int)str.charAt(i) - 65] += 1;
        }
        for (int i = 0; i < str.length(); ++i) {
            if (1 == mapping[(int)str.charAt(i) - 65]) {
                return str.charAt(i);
            }
        }
        return 'x'; // Dummy return
    }
}