// https://www.lintcode.com/problem/maximum-and-minimum/

public class Solution {
    /**
     * @param matrix: an input matrix 
     * @return: nums[0]: the maximum,nums[1]: the minimum
     */
    public int[] maxAndMin(int[][] matrix) {
        // write your code here
        if (matrix.length == 0) {
            return new int[]{};
        }
        int[] ret = new int[]{matrix[0][0], matrix[0][0]};
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[i].length; ++j) {
                if (matrix[i][j] < ret[1]) {
                    ret[1] = matrix[i][j];
                }
                if (matrix[i][j] > ret[0]) {
                    ret[0] = matrix[i][j];
                }
            }
        }
        return ret;
    }
}