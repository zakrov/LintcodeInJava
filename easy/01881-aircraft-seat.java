// https://www.lintcode.com/problem/aircraft-seat/

public class Solution {
    /**
     * @param N:  the number of rows
     * @param S: a list of reserved seats
     * @return: nothing
     */
    public int solution(int N, String S) {
        // Write your code here.
        int ret = 0;
        String[] occupied = S.split(" ");
        Set<String> set = new HashSet<>();
        for (String s : occupied) {
            set.add(s);
        }
        for (int i = 1; i <= N; ++i) {
            String sb = i + "B";
            String sc = i + "C";
            String sd = i + "D";
            String se = i + "E";
            String sf = i + "F";
            String sg = i + "G";
            String sh = i + "H";
            String si = i + "I";
            if ((!set.contains(sb)) && (!set.contains(sc)) && (!set.contains(sd)) && (!set.contains(se))) {
                ++ret;
                set.add(se);
            }
            if ((!set.contains(sd)) && (!set.contains(se)) && (!set.contains(sf)) && (!set.contains(sg))) {
                ++ret;
                set.add(sg);
            }
            if ((!set.contains(sf)) && (!set.contains(sg)) && (!set.contains(sh)) && (!set.contains(si))) {
                ++ret;
            }
        }
        return ret;
    }
}