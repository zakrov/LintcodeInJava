// https://www.lintcode.com/problem/string-compression/

public class Solution {
    /**
     * @param originalString: a string
     * @return: a compressed string
     */
    public String compress(String originalString) {
        // write your code here
        StringBuilder sb = new StringBuilder("");
        char prev_c = '\0';
        int count = 0;
        for (int i = 0; i < originalString.length(); ++i) {
            char c = originalString.charAt(i);
            if (c == prev_c) {
                ++count;
            }
            else {
                if (prev_c != '\0') {
                    sb.append(prev_c);
                    sb.append(String.valueOf(count));
                    prev_c =  c;
                    count = 1;
                }
                prev_c = c;
                count = 1;
            }
        }
        if (count > 0) {
            sb.append(prev_c);
            sb.append(String.valueOf(count));
        }
        if (sb.toString().length() < originalString.length()) {
            return sb.toString();
        }
        else {
            return originalString;
        }
    }
}