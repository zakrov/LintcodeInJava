// https://www.lintcode.com/problem/reshape-the-matrix/

public class Solution {
    /**
     * @param nums: List[List[int]]
     * @param r: an integer
     * @param c: an integer
     * @return: return List[List[int]]
     */
    public int[][] matrixReshape(int[][] nums, int r, int c) {
        // write your code here
        if ((r * c) != (nums.length * nums[0].length)) {
            return nums;
        }
        int[][] ret = new int[r][c];
        for (int i = 0; i < nums.length; ++i) {
            for (int j = 0; j < nums[0].length; ++j) {
                int idx = i * nums[0].length + j;
                ret[idx / c][idx % c] = nums[i][j];
            }
        }
        return ret;
    }
}