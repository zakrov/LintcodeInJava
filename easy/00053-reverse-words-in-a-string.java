// https://www.lintcode.com/problem/reverse-words-in-a-string/

public class Solution {
    /*
     * @param s: A string
     * @return: A string
     */
    public String reverseWords(String s) {
        // write your code here
        String[] ss = s.split(" ");
        int start = 0, end = ss.length - 1;
        while (start < end) {
            String tmp = ss[start];
            ss[start] = ss[end];
            ss[end] = tmp;
            ++start;
            --end;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ss.length; ++i) {
            if (ss[i].equals("")) {
                continue;
            }
            sb.append(ss[i]).append(" ");
        }
        if (sb.length() != 0) {
            return sb.substring(0, sb.length() - 1);
        }
        return "";
    }
}