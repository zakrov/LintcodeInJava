// https://www.lintcode.com/problem/unique-array/

public class Solution {
    /**
     * @param arr: a integer array
     * @return: return the unique array
     */
    public int[] getUniqueArray(int[] arr) {
        // write your code here
        Set<Integer> set = new HashSet<>();
        int dups = 0;
        for (int i = 0; i < arr.length; ++i) {
            int v = arr[i];
            if (!set.contains(v)) {
                set.add(v);
                arr[i - dups] = v;
            } else {
                ++dups;
            }
        }
        int[] ret = new int[arr.length - dups];
        for (int i = 0; i < ret.length; ++i) {
            ret[i] = arr[i];
        }
        return ret;
    }
}