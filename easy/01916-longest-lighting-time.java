// https://www.lintcode.com/problem/longest-lighting-time/

public class Solution {
    /**
     * @param operation: A list of operations.
     * @return: The lamp has the longest liighting time.
     */
    public char longestLightingTime(List<List<Integer>> operation) {
        // write your code here
        int ret = 0;
        Integer lst = 0;
        Integer pt = 0;
        for (List<Integer> op : operation) {
            Integer t = op.get(1) - pt;
            if (t > lst) {
                lst = t;
                ret = op.get(0);
            }
            pt = op.get(1);
        }
        return (char)('a' + ret);
    }
}