// https://www.lintcode.com/problem/paint-fill/

public class Solution {
    /**
     * @param screen: a two-dimensional array of colors
     * @param x: the abscissa of the specified point
     * @param y: the ordinate of the specified point
     * @param newColor: the specified color
     * @return: Can it be filled
     */
    public boolean paintFill(int[][] screen, int x, int y, int newColor) {
        // write your code here.
        if (screen[x][y] == newColor) {
            return false;
        }
        int old_color = screen[x][y];
        screen[x][y] = newColor;
        if (x > 0) { // 左
            if (screen[x - 1][y] == old_color) {
                paintFill(screen, x - 1, y, newColor);
            }
        }
        if (x < (screen.length - 1)) { // 右
            if (screen[x + 1][y] == old_color) {
                paintFill(screen, x + 1, y, newColor);
            }
        }
        if (y > 0) { // 下
            if (screen[x][y - 1] == old_color) {
                paintFill(screen, x, y - 1, newColor);
            }
        }
        if (y < (screen[0].length - 1)) { // 上
            if (screen[x][y + 1] == old_color) {
                paintFill(screen, x, y + 1, newColor);
            }
        }
        return true;
    }
}