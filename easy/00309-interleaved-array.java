// https://www.lintcode.com/problem/interleaved-array/

public class Solution {
    /**
     * @param A: the array A
     * @param B: the array B
     * @return: returns an alternate array of arrays A and B.
     */
    public int[] interleavedArray(int[] A, int[] B) {
        // Interleaved Array
        int[] ret = new int[A.length * 2];
        for (int i = 0; i < A.length; ++i) {
            ret[i * 2] = A[i];
            ret[i * 2 + 1] = B[i];
        }
        return ret;
    }
}