// https://www.lintcode.com/problem/rotated-digits/

public class Solution {
    /**
     * @param N: a positive number
     * @return: how many numbers X from 1 to N are good
     */
    public int rotatedDigits(int N) {
        // write your code here
        int ret = 0;
        for (int i = 1; i <= N; ++i) {
            int r = 0, j = i;
            while (j > 0) {
                int m = j % 10;
                j /= 10;
                if ((m == 3) || (m == 4) || (m == 7)) {
                    r = 0;
                    break;
                } else if ((m == 2) || (m == 5) || (m == 6) || (m == 9)) {
                    r = 1;
                }
            }
            ret += r;
        }
        return ret;
    }
}