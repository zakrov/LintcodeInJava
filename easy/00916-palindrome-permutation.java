// https://www.lintcode.com/problem/palindrome-permutation/

public class Solution {
    /**
     * @param s: the given string
     * @return: if a permutation of the string could form a palindrome
     */
    public boolean canPermutePalindrome(String s) {
        // write your code here
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); ++i) {
            Character c = s.charAt(i);
            if (!map.containsKey(c)) {
                map.put(c, 0);
            }
            map.put(c, map.get(c) + 1);
        }
        int none_pairs = 0; // 非配对数目
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if ((entry.getValue() % 2) != 0) {
                ++none_pairs;
            }
        }
        if ((s.length() % 2) == 0) {
            return none_pairs == 0; // 不能有落单的
        } else {
            return none_pairs == 1;
        }
    }
}