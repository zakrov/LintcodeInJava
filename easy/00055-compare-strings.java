// https://www.lintcode.com/problem/compare-strings/

public class Solution {
    /**
     * @param A: A string
     * @param B: A string
     * @return: if string A contains all of the characters in B return true else return false
     */
    public boolean compareStrings(String A, String B) {
        // write your code here
        HashMap<Character, Integer> map = new HashMap<>(); // HashMap不能用基本类型
        for (int i = 0; i < A.length(); ++i) {
            char c = A.charAt(i);
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            }
            else {
                map.put(c, 1);
            }
        }
        for (int i = 0; i < B.length(); ++i) {
            char c = B.charAt(i);
            if (map.containsKey(c)) {
                if (map.get(c) <= 0) {
                    return false;
                }
                else {
                    map.put(c, map.get(c) - 1);
                }
            }
            else {
                return false;
            }
        }
        return true;
    }
}