// https://www.lintcode.com/problem/guess-number-higher-or-lower/

/* The guess API is defined in the parent class GuessGame.
   @param num, your guess
   @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
      int guess(int num); */

public class Solution extends GuessGame {
    /**
     * @param n an integer
     * @return the number you guess
     */
    public int guessNumber(int n) {
        // Write your code here
        int start = 1, end = n;
        while (true) {
            int val = (int)((start + end) / 2);
            int r = guess(val);
            if (r == 0) {
                return val;
            } else if (r < 0) {
                end = val - 1;
            } else {
                start = val + 1;
            }
        }
    }
}