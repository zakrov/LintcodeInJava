// https://www.lintcode.com/problem/remove-arbitrary-space/

public class Solution {
    /**
     * @param s: the original string
     * @return: the string without arbitrary spaces
     */
    public String removeExtra(String s) {
        // write your code here
        StringBuilder ret = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (ret.length() == 0) {
                if (c != ' ') {
                    ret.append(c);
                }
            } else {
                if (c != ' ') {
                    ret.append(c);
                } else {
                    if (c != ret.charAt(ret.length() - 1)) {
                        ret.append(c);
                    }
                }
            }
        }
        while ((ret.length() > 0) && (ret.charAt(ret.length() - 1) == ' ')) { // 不用strip
            ret.deleteCharAt(ret.length() - 1);
        }
        return ret.toString();
    }
}