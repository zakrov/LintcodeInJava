// https://www.lintcode.com/problem/johns-backyard-garden/

public class Solution {
    /**
     * @param x: the wall's height
     * @return: YES or NO
     */
    public String isBuild(int x) {
        // write you code here
        int m = x % 7;
        if ((m == 0) || (m == 3) || (m == 6)) {
            return "YES";
        } else if ((x > 7) && ((m == 5) || (m == 2))) {
            return "YES";
        }
        return "NO";
    }
}