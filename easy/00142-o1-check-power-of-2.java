// https://www.lintcode.com/problem/o1-check-power-of-2/

public class Solution {
    /**
     * @param n: An integer
     * @return: True or false
     */
    public boolean checkPowerOf2(int n) {
        // write your code here
        if (n <= 0) {
            return false;
        }
        return 0 == (n & (n - 1));
    }
}