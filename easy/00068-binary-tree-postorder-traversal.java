// https://www.lintcode.com/problem/binary-tree-postorder-traversal/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param root: A Tree
     * @return: Postorder in ArrayList which contains node values.
     */
    public List<Integer> postorderTraversal(TreeNode root) {
        // write your code here
        List<Integer> ret = new ArrayList<>();
        postorderTraversal(root, ret);
        return ret;
    }
    
    public void postorderTraversal(TreeNode root, List<Integer> ret) {
        if (root != null) {
            postorderTraversal(root.left, ret);
            postorderTraversal(root.right, ret);
            ret.add(root.val);
        }
    }
}