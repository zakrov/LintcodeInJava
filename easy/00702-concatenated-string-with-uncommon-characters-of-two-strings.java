// https://www.lintcode.com/problem/concatenated-string-with-uncommon-characters-of-two-strings/

public class Solution {
    /**
     * @param s1: the 1st string
     * @param s2: the 2nd string
     * @return: uncommon characters of given strings
     */
    public String concatenetedString(String s1, String s2) {
        // write your code here
        StringBuilder ret = new StringBuilder();
        HashSet<Character> set_1 = buildSet(s1);
        HashSet<Character> set_2 = buildSet(s2);
        updateSB(ret, s1, set_2);
        updateSB(ret, s2, set_1);
        return ret.toString();
    }
    
    protected HashSet<Character> buildSet(String s) {
        HashSet<Character> set = new HashSet<>();
        for (int i = 0; i < s.length(); ++i) {
            set.add(s.charAt(i));
        }
        return set;
    }
    
    protected void updateSB(StringBuilder sb, String s, HashSet<Character> set) {
        for (int i = 0; i < s.length(); ++i) {
            if (!set.contains(s.charAt(i))) {
                sb.append(s.charAt(i));
            }
        }
    }
}