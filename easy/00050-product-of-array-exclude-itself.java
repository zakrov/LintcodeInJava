// https://www.lintcode.com/problem/product-of-array-exclude-itself/

public class Solution {
    /*
     * @param nums: Given an integers array A
     * @return: A long long array B and B[i]= A[0] * ... * A[i-1] * A[i+1] * ... * A[n-1]
     */
    public List<Long> productExcludeItself(List<Integer> nums) {
        // write your code here
        List<Long> ret = new ArrayList<>(Collections.nCopies(nums.size(),1L));
        for (int i = 0; i < nums.size(); ++i) {
            for (int j = 0; j < nums.size(); ++j) {
                if (i != j) {
                    ret.set(i, ret.get(i) * nums.get(j).longValue());
                }
            }
        }
        return ret;
    }
}