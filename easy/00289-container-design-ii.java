// https://www.lintcode.com/problem/container-design-ii/

public class MyContainerII {
    public MyContainerII() {
        // initialize your data structure here
        list = new ArrayList<>();
        sum = 0;
        
    }
    
    /**
     * @param element: Add element into this container.
     * @return: nothing
     */
    public void add(int element) {
        // write your code here
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) == element) {
                return;
            }
        }
        list.add(element);
        sum += element;
    }

    /**
     * @param element: Remove element into this container.
     * @return: nothing
     */
    public void remove(int element) {
        // write your code here
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) == element) {
                list.remove(i);
                sum -= element;
                return;
            }
        }
    }

    /**
     * @return: Sum of integers.
     */
    public int getSum() {
        // write your code here
        return sum;
    }
    
    protected List<Integer> list;
    int sum;
}