// https://www.lintcode.com/problem/hail-conjecture/

public class Solution {
    /**
     * @param num: an integer
     * @return: return an integer
     */
    public int getAnswer(int num) {
        // write your code here.
        int ret = 0;
        while (num != 1) {
            if ((num % 2) == 0) {
                num /= 2;
            } else {
                num = num * 3 + 1;
            }
            ++ret;
        }
        return ret;
    }
}