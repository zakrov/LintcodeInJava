https://www.lintcode.com/problem/second-max-of-array/

public class Solution {
    /**
     * @param nums: An integer array
     * @return: The second max number in the array.
     */
    public int secondMax(int[] nums) {
        // write your code here
        int min = Math.min(nums[0], nums[1]), max = Math.max(nums[0], nums[1]);
        for (int i = 2; i < nums.length; ++i) {
            if (nums[i] > max) {
                min = max;
                max = nums[i];
            }
            else if (nums[i] > min) {
                min = nums[i];
            }
        }
        return min;
    }
}