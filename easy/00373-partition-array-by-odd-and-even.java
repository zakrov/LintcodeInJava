// https://www.lintcode.com/problem/partition-array-by-odd-and-even/

public class Solution {
    /*
     * @param nums: an array of integers
     * @return: nothing
     */
    public void partitionArray(int[] nums) {
        // write your code here
        int i = 0, j = nums.length - 1;
        while (i < j) {
            if ((0 == (nums[i] % 2)) && (1 == (nums[j] % 2))) {
                int tmp = nums[i];
                nums[i] = nums[j];
                nums[j] = tmp;
                ++i;
                --j;
            }
            else {
                if (1 == (nums[i] % 2)) {
                    ++i;
                }
                if (0 == (nums[j] % 2)) {
                    --j;
                }
            }
        }
    }
}