// https://www.lintcode.com/problem/same-diagonal-elements/

public class Solution {
    /**
     * @param matrix: a matrix
     * @return: return true if same.
     */
    public boolean judgeSame(int[][] matrix) {
        // write your code here.
        int n = matrix.length;
        for (int i = 0; i < n; ++i) {
            int r = 1, c = i + 1;
            while ((r < n) && (c < n)) {
                if (matrix[r][c] != matrix[0][i]) {
                    return false;
                }
                ++r;
                ++c;
            }
        }
        for (int i = 1; i < n; ++i) {
            int r = i + 1, c = 1;
            while ((r < n) && (c < n)) {
                if (matrix[r][c] != matrix[i][0]) {
                    return false;
                }
                ++r;
                ++c;
            }
        }
        return true;
    }
}