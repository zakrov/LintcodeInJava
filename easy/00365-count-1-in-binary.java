// https://www.lintcode.com/problem/count-1-in-binary/

public class Solution {
    /*
     * @param num: An integer
     * @return: An integer
     */
    public int countOnes(int num) {
        // write your code here
        int ret = 0;
        while(num!=0){
            num = num & (num-1);
            ++ret;
        }
        return ret;
    }
}