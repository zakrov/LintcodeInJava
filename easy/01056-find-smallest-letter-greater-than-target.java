// https://www.lintcode.com/problem/find-smallest-letter-greater-than-target/

public class Solution {
    /**
     * @param letters: a list of sorted characters
     * @param target: a target letter
     * @return: the smallest element in the list that is larger than the given target
     */
    public char nextGreatestLetter(char[] letters, char target) {
        // Write your code here
        HashSet<Character> set = new HashSet();
        for (char c : letters) {
            set.add(c);
        }
        while (true) {
            if (target == 'z') {
                target = 'a';
            } else {
                ++target;
            }
            if (set.contains(target)) {
                return target;
            }
            
        }
    }
}