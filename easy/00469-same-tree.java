// https://www.lintcode.com/problem/same-tree/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param a: the root of binary tree a.
     * @param b: the root of binary tree b.
     * @return: true if they are identical, or false.
     */
    public boolean isIdentical(TreeNode a, TreeNode b) {
        // write your code here
        if ((a != null) && (b != null)) {
            if (a.val == b.val) {
                boolean identical = isIdentical(a.left, b.left);
                if (identical) {
                    identical = isIdentical(a.right, b.right);
                }
                return identical;
            }
        }
        else if ((null == a) && (null == b)) {
            return true;
        }
        return false;
    }
}