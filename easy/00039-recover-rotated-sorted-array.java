// https://www.lintcode.com/problem/recover-rotated-sorted-array/

public class Solution {
    /**
     * @param nums: An integer array
     * @return: nothing
     */
    public void recoverRotatedSortedArray(List<Integer> nums) {
        // write your code here
        if (nums.get(0) < nums.get(nums.size() - 1)) {
            return;
        }
        int point = 0;
        for (int i = 0; i < nums.size() - 1; ++i) {
            if (nums.get(i) > nums.get(i + 1)) {
                point = i + 1;
                break;
            }
        }
        reverse(nums, 0, point - 1);
        reverse(nums, point, nums.size() - 1);
        reverse(nums, 0, nums.size() - 1);
    }
    
    private void reverse(List<Integer> nums, int start, int end) {
        while (start < end) {
            int a = nums.get(start);
            nums.set(start, nums.get(end));
            nums.set(end, a);
            ++start;
            --end;
        }
    }
}