// https://www.lintcode.com/problem/jewels-and-stones/

public class Solution {
    /**
     * @param J: the types of stones that are jewels
     * @param S: representing the stones you have
     * @return: how many of the stones you have are also jewels
     */
    public int numJewelsInStones(String J, String S) {
        // Write your code here
        int ret = 0;
        HashSet<Character> set = new HashSet<>();
        for (int i = 0; i < J.length(); ++i) {
            set.add(J.charAt(i));
        }
        for (int i = 0; i < S.length(); ++i) {
            if (set.contains(S.charAt(i))) {
                ++ret;
            }
        }
        return ret;
    }
}