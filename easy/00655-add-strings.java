https://www.lintcode.com/problem/add-strings/

public class Solution {
    /**
     * @param num1: a non-negative integers
     * @param num2: a non-negative integers
     * @return: return sum of num1 and num2
     */
    public String addStrings(String num1, String num2) {
        // write your code here
        StringBuilder ret = new StringBuilder();
        int l_1 = num1.length() - 1, l_2 = num2.length() - 1;
        int step = 0;
        while ((l_1 >= 0) && (l_2 >= 0)) {
            int v = (num1.charAt(l_1) - '0') + (num2.charAt(l_2) - '0') + step;
            if (v >= 10) {
                v = v - 10;
                step = 1;
            } else {
                step = 0;
            }
            ret.append((char)('0' + v));
            --l_1;
            --l_2;
        }
        int l = (l_1 < 0) ? l_2 : l_1;
        String s = (l_1 < 0) ? num2 : num1;
        while (l >= 0) {
            int v = (s.charAt(l) - '0') + step;
            if (v >= 10) {
                v -= 10;
                step = 1;
            } else {
                step = 0;
            }
            ret.append((char)('0' + v));
            --l;
        }
        if (step > 0) {
            ret.append((char)('0' + step));
        }
        return ret.reverse().toString();
    }
}