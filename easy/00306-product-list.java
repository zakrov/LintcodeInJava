// https://www.lintcode.com/problem/product-list/

public class Solution {
    /**
     * @param offset: the number of items that the customer has viewed
     * @param n: the number of items that can be displayed on a page
     * @param len1: the length of L1
     * @param len2: the length of L2
     * @return: returns the intervals of goods displayed in L1 and L2
     */
    public List<Integer> ProductList(int offset, int n, int len1, int len2) {
        // write your code here
        List<Integer> ret = new ArrayList<>();
        int total = offset + n;
        if (offset < len1) {
            ret.add(offset);
            if (total <= len1) {
                ret.add(total);
            } else {
                ret.add(len1);
            }
        } else {
            ret.add(len1);
            ret.add(len1);
        }
        if (total >= len1) {
            if (offset >= len1) {
                ret.add(offset - len1);
            } else {
                ret.add(0);
            }
            total -= len1;
            ret.add(Math.min(total, len2));
        } else {
            ret.add(0);
            ret.add(0);
        }
        return ret;
    }
}