// https://www.lintcode.com/problem/binary-tree-inorder-traversal/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param root: A Tree
     * @return: Inorder in ArrayList which contains node values.
     */
    public List<Integer> inorderTraversal(TreeNode root) {
        // write your code here
        List<Integer> ret = new ArrayList<>();
        inorderTraversal(root, ret);
        return ret;
    }
    
    public void inorderTraversal(TreeNode root, List<Integer> ret) {
        if (root != null) {
            inorderTraversal(root.left, ret);
            ret.add(root.val);
            inorderTraversal(root.right, ret);
        }
    }
}