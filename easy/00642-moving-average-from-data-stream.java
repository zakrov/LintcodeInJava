// https://www.lintcode.com/problem/moving-average-from-data-stream/

public class MovingAverage {
    int size;
    int[] queue;
    double total;
    int pos, count;
    
    /*
    * @param size: An integer
    */public MovingAverage(int size) {
        // do intialization if necessary
        this.size = size;
        queue = new int[size];
        total = 0.0f;
        pos = 0;
        count = 0; // 窗口是否填满
    }

    /*
     * @param val: An integer
     * @return:  
     */
    public double next(int val) {
        // write your code here
        total += val;
        if (count == size) {
            pos %= size;
            total -= queue[pos];
            queue[pos] = val;
            pos += 1;
            
        } else {
            queue[pos] = val;
            pos += 1;
            count += 1;
        }
        return total / (double)count;
    }
}