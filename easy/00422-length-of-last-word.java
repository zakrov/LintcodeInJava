// https://www.lintcode.com/problem/length-of-last-word/

public class Solution {
    /**
     * @param s: A string
     * @return: the length of last word
     */
    public int lengthOfLastWord(String s) {
        // write your code here
        int ret = 0, i = s.length() - 1;
        while (i >= 0) {
            if (s.charAt(i) != ' ') {
                ++ret;
            }
            else if (ret > 0) { // 遇到空格
                break;
            }
            --i;
        }
        return ret;
    }
}