// https://www.lintcode.com/problem/unique-morse-code-words/

public class Solution {
    /**
     * @param words: the given list of words
     * @return: the number of different transformations among all words we have
     */
    public int uniqueMorseRepresentations(String[] words) {
        // Write your code here
        String[] table = new String[]{".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};
        HashSet<String> set = new HashSet();
        for (String word : words) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < word.length(); ++i) {
                char c = word.charAt(i);
                sb.append(table[c - 'a']);
            }
            String code = sb.toString();
            if (!set.contains(code)) {
                set.add(code);
            }
        }
        return set.size();
    }
}