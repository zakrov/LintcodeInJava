// https://www.lintcode.com/problem/subarray-sum/

public class Solution {
    /**
     * @param nums: A list of integers
     * @return: A list of integers includes the index of the first number and the index of the last number
     */
    public List<Integer> subarraySum(int[] nums) {
        // write your code here
        for (int i = 0; i < nums.length; ++i) {
            int sum = 0;
            for (int j = i; j < nums.length; ++j) {
                sum += nums[j];
                if (0 == sum) {
                    return new ArrayList<Integer>(Arrays.asList(i, j));
                }
            }
        }
        return new ArrayList<Integer>(Arrays.asList(1, 2)); // Dummy return
    }
}