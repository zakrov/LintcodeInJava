// https://www.lintcode.com/problem/student-attendance-record-i/

public class Solution {
    /**
     * @param s: a string
     * @return: whether the student could be rewarded according to his attendance record
     */
    public boolean checkRecord(String s) {
        // Write your code here
        char pl = ' ';
        int ca = 0, cl = 0;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (c == 'L') {
                if (pl != 'L') {
                    cl = 1;
                } else {
                    ++cl;
                    if (cl == 3) {
                        return false;
                    }
                }
            } else {
                cl = 0;
                if (c == 'A') {
                    ++ca;
                    if (ca > 1) {
                        return false;
                    }
                }
            }
            pl = c;
        }
        return true;
    }
}