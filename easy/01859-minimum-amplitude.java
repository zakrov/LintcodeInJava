// https://www.lintcode.com/problem/minimum-amplitude/

public class Solution {
    /**
     * @param A: a list of integer
     * @return: Return the smallest amplitude
     */
    public int MinimumAmplitude(int[] A) {
        // write your code here
        int ret = 0;
        Arrays.sort(A);
        if (A.length > 3) {
            int i = A.length - 1;
            ret = A[i] - A[3]; // 去掉前面3个
            ret = Math.min(A[i - 1] - A[2], ret); // 比较去掉前面2个和最后1个的情况
            ret = Math.min(A[i - 2] - A[1], ret); // 比较去掉前面1个和最后2个的情况
            ret = Math.min(A[i - 3] - A[0], ret); // 比较去掉最后3个的情况
        }
        return ret;
    }
}