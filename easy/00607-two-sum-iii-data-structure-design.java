// https://www.lintcode.com/problem/two-sum-iii-data-structure-design/

public class TwoSum {
    protected HashMap<Integer, Integer> map;
    
    TwoSum() {
        map = new HashMap<>();
    }
    /**
     * @param number: An integer
     * @return: nothing
     */
    public void add(int number) {
        // write your code here
        if (!map.containsKey(number)) {
            map.put(number, 0);
        }
        map.put(number, map.get(number) + 1);
    }

    /**
     * @param value: An integer
     * @return: Find if there exists any pair of numbers which sum is equal to the value.
     */
    public boolean find(int value) {
        // write your code here
        for (Integer key : map.keySet()) {
            Integer target = value - key;
            if (key == target) {
                if (map.get(key) >= 2) {
                    return true;
                }
            } else {
                if (map.containsKey(target)) {
                    return true;
                }
            }
        }
        return false;
    }
}