// https://www.lintcode.com/problem/stretch-word/

public class Solution {
    /**
     * @param S: the string
     * @return: The numbers of strings
     */
    public long stretchWord(String S) {
        // write your code here
        long ret = 0;
        Character pc = '\0';
        int cnt = 0;
        for (int i = 0; i < S.length(); ++i) {
            Character c = S.charAt(i);
            if (c != pc) {
                pc = c;
                cnt = 0;
                if (ret == 0) {
                    ret = 1;
                }
            } else {
                if (cnt >= 1) {
                    continue;
                }
                ++cnt;
                ret *= 2;
            }
        }
        return ret;
    }
}