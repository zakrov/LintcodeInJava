// https://www.lintcode.com/problem/valid-word-abbreviation/

public class Solution {
    /**
     * @param word: a non-empty string
     * @param abbr: an abbreviation
     * @return: true if string matches with the given abbr or false
     */
    public boolean validWordAbbreviation(String word, String abbr) {
        // write your code here
        int wlen = word.length(), alen = abbr.length();
        if (alen > wlen) {
            return false;
        }
        int pos = 0, step = 0;
        for (int i = 0; i < alen; ++i) {
            char c = abbr.charAt(i);
            if ((c < '0') || (c > '9')) {
                pos += step;
                if (c != word.charAt(pos)) {
                    return false;
                }
                pos += 1;
                step = 0;
            } else {
                step = step * 10 + (int)(c - '0');
                if ((pos + step) > wlen) {
                    return false;
                }
            }
        }
        return true;
     }
}