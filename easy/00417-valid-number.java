// https://www.lintcode.com/problem/valid-number/

public class Solution {
    /**
     * @param s: the string that represents a number
     * @return: whether the string is a valid number
     */
    public boolean isNumber(String s) {
        // write your code here
        s = s.trim();
        if (s.length() == 0) {
            return false;
        }
        int i = 0;
        if (('+' == s.charAt(i)) || ('-' == s.charAt(i))) {
            ++i;
        }
        boolean number = false, exp = false, dot = false;
        while (i < s.length()) {
            char ch = s.charAt(i);
            if ((ch >= '0') && (ch <= '9')) {
                number = true;
            }
            else if ('.' == ch) {
                if (dot || exp) { // e后面只跟整数
                    return false;
                }
                dot = true;
            }
            else if ('e' == ch) {
                if ((!number) || exp || dot) {
                    return false;
                }
                number = false; // e后面一定要跟数字
                exp = true;
            }
            else {
                return false;
            }
            ++i;
        }
        return number;
    }
}