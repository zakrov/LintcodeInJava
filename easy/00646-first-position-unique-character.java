// https://www.lintcode.com/problem/first-position-unique-character/

public class Solution {
    /**
     * @param s: a string
     * @return: it's index
     */
    public int firstUniqChar(String s) {
        // write your code here
        int ret = s.length();
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); ++i) {
            Character c = s.charAt(i);
            if (map.containsKey(c)) {
                map.put(c, -1);
            } else {
                map.put(c, i);
            }
        }
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            int pos = entry.getValue();
            if ((pos != -1) && (pos < ret)) {
                ret = pos;
            }
        }
        if (ret == s.length()) {
            return -1;
        }
        return ret;
    }
}