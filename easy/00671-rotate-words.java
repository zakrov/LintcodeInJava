// https://www.lintcode.com/problem/rotate-words/

public class Solution {
    /*
     * @param words: A list of words
     * @return: Return how many different rotate words
     */
    public int countRotateWords(List<String> words) {
        // Write your code here
        int ret = 0;
        for (int i = 0; i < words.size(); ++i) {
            String wi = words.get(i);
            if (!wi.equals("")) {
                ret += 1;
                String s = wi + wi;
                for (int j = i + 1; j < words.size(); ++j) {
                    String wj = words.get(j);
                    if (!wj.equals("")) {
                        if (((wj.length() * 2) == s.length()) && (s.indexOf(wj) >= 0)) {
                            words.set(j, "");
                        }
                    }
                }
            }
        }
        return ret;
    }
}