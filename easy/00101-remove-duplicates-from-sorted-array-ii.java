// https://www.lintcode.com/problem/remove-duplicates-from-sorted-array-ii/

public class Solution {
    /**
     * @param A: a array of integers
     * @return : return an integer
     */
    public int removeDuplicates(int[] nums) {
        // write your code here
        int dups = 0, cnt = 0;
        for (int i = 1; i < nums.length; ++i) {
            if (nums[i] == nums[i - 1]) {
                if (cnt < 2) {
                    ++cnt;
                }
                if (2 == cnt) {
                    ++dups;
                }
            }
            else {
                cnt = 0;
            }
            nums[i - dups] = nums[i];
        }
        return nums.length - dups;
    }
}