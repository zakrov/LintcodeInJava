// https://www.lintcode.com/problem/sum-of-all-subsets/

public class Solution {
    /**
     * @param n: the given number
     * @return: Sum of elements in subsets
     */
    public int subSum(int n) {
        // write your code here
        return (n == 1) ? 1 : ((1 << (n - 2)) * (1 + n) * n);
    }
}