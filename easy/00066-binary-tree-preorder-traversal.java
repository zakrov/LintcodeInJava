// https://www.lintcode.com/problem/binary-tree-preorder-traversal/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param root: A Tree
     * @return: Preorder in ArrayList which contains node values.
     */
    public List<Integer> preorderTraversal(TreeNode root) {
        // write your code here
        List<Integer> ret = new ArrayList<>();
        preorderTraversal(root, ret);
        return ret;
    }
    
    public void preorderTraversal(TreeNode root, List<Integer> ret) {
        // write your code here
        if (root != null) {
            ret.add(root.val);
            preorderTraversal(root.left, ret);
            preorderTraversal(root.right, ret);
        }
    }
}