https://www.lintcode.com/problem/capitalizes-the-first-letter/

public class Solution {
    /**
     * @param s: a string
     * @return: a string after capitalizes the first letter
     */
    public String capitalizesFirst(String s) {
        // Write your code here
        StringBuilder sb = new StringBuilder();
        sb.append(s);
        boolean first = true;
        for (int i = 0; i < sb.length(); ++i) {
            if ((s.charAt(i) != ' ') && first) {
                sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));
                first = false;
            } else if (sb.charAt(i) == ' ') {
                first = true;
            }
        }
        return sb.toString();
    }
}