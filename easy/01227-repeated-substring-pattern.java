// https://www.lintcode.com/problem/repeated-substring-pattern/

public class Solution {
    /**
     * @param s: a string
     * @return: return a boolean
     */
    public boolean repeatedSubstringPattern(String s) {
        // write your code here
        // 简单的做法内存会爆
        String ns = s.substring(1);
        ns += s.substring(0, s.length() - 1);
        return ns.contains(s);
    }
}