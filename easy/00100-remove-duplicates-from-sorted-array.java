// https://www.lintcode.com/problem/remove-duplicates-from-sorted-array/

public class Solution {
    /*
     * @param nums: An ineger array
     * @return: An integer
     */
    public int removeDuplicates(int[] nums) {
        // write your code here
        int dups = 0;
        for (int i = 1; i < nums.length; ++i) {
            if (nums[i] == nums[i - 1]) {
                ++dups;
            }
            nums[i - dups] = nums[i];
            
        }
        return nums.length - dups;
    }
}