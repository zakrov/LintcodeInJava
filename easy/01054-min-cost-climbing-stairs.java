// https://www.lintcode.com/problem/min-cost-climbing-stairs/

public class Solution {
    /**
     * @param cost: an array
     * @return: minimum cost to reach the top of the floor
     */
    public int minCostClimbingStairs(int[] cost) {
        // Write your code here
        int[] ret = new int[cost.length + 1];
        Arrays.fill(ret, 0);
        for (int i = 2; i < ret.length; ++i) {
            ret[i] = Math.min(ret[i - 1] + cost[i - 1], ret[i - 2] + cost[i - 2]);
        }
        return ret[ret.length - 1];
    }
}