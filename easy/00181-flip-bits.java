// https://www.lintcode.com/problem/flip-bits/

public class Solution {
    /**
     * @param a: An integer
     * @param b: An integer
     * @return: An integer
     */
    public int bitSwapRequired(int a, int b) {
        // write your code here
        int x = a ^ b;
        if (x >= 0) {
            return ones(x);
        }
        else {
            return 32 - ones(Math.abs(x) - 1);
        }
    }
    
    private int ones(int x) {
        int ret = 0, b = 1;
        for (int i = 0; i < 32; ++i) {
            if ((x & b) > 0) {
                ++ret;
            }
            b <<= 1;
        }
        return ret;
    }
}