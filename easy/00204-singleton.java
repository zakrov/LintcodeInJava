// https://www.lintcode.com/problem/singleton/

class Solution {
    /**
     * @return: The same instance of this class every time
     */
    public static Solution getInstance() { // 实际场景要考虑并发
        // write your code here
        if (null == s) {
            s = new Solution();
        }
        return s;
    }
    
    private static Solution s = null;
};