// https://www.lintcode.com/problem/majority-element/

public class Solution {
    /*
     * @param nums: a list of integers
     * @return: find a  majority number
     */
    public int majorityNumber(List<Integer> nums) {
        // write your code here
        int ret = 0, count = 0;
        for (int i = 0; i < nums.size(); ++i) {
            if (count == 0) {
                ret = nums.get(i);
                count = 1;
            }
            else {
                if (ret == nums.get(i)) {
                    ++count;
                }
                else {
                    --count;
                }
            }
        }
        return ret;
    }
}