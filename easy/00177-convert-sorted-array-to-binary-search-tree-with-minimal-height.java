// https://www.lintcode.com/problem/convert-sorted-array-to-binary-search-tree-with-minimal-height/descriptionhttps://www.lintcode.com/problem/convert-sorted-array-to-binary-search-tree-with-minimal-height/

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */


public class Solution {
    /*
     * @param A: an integer array
     * @return: A tree node
     */
    public TreeNode sortedArrayToBST(int[] A) {
        // write your code here
        return _sortedArrayToBST(A, 0, A.length - 1);
    }
    
    private TreeNode _sortedArrayToBST(int[] A, int start, int end) {
        if (start > end) {
            return null;
        }
        int mid = (start + end) / 2;
        TreeNode root = new TreeNode(A[mid]);
        root.left = _sortedArrayToBST(A, start, mid - 1);
        root.right = _sortedArrayToBST(A, mid + 1, end);
        return root;
    }
}