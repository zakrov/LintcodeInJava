// https://www.lintcode.com/problem/hex-conversion/

public class Solution {
    /**
     * @param n: a decimal number
     * @param k: a Integer represent base-k
     * @return: a base-k number
     */
    public String hexConversion(int n, int k) {
        // write your code here
        StringBuilder sb = new StringBuilder();
        while (n >= k) {
            appendDigit(sb, n % k);
            n /= k;
        }
        if ((n > 0) || (sb.length() == 0)) {
            appendDigit(sb, n);
        }
        return sb.reverse().toString();
    }
    
    protected void appendDigit(StringBuilder sb, int i) {
        if (i < 10) {
            sb.append((char)(i + '0'));
        } else {
            sb.append((char)(i - 10 + 'A'));
        }
    }
}