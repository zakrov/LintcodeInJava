// https://www.lintcode.com/problem/baseball-game/my-submissions

public class Solution {
    /**
     * @param ops: the list of operations
     * @return:  the sum of the points you could get in all the rounds
     */
    public int calPoints(String[] ops) {
        // Write your code here
        List<Integer> scores = new ArrayList<>();
        for (int i = 0; i < ops.length; ++i) {
            String op = ops[i];
            if (op.equals("+")) {
                int v1 = scores.get(scores.size() - 1);
                int v2 = scores.get(scores.size() - 2);
                scores.add(v1 + v2);
            } else if (op.equals("C")) {
                int pos = scores.size() - 1;
                scores.remove(pos);
            } else if (op.equals("D")) {
                int v1 = scores.get(scores.size() - 1);
                scores.add(v1 * 2);
            } else {
                scores.add(Integer.parseInt(op));
            }
        }
        return scores.stream().mapToInt(Integer::intValue).sum();
    }
}