// https://www.lintcode.com/problem/window-sum/

public class Solution {
    /**
     * @param nums: a list of integers.
     * @param k: length of window.
     * @return: the sum of the element inside the window at each moving.
     */
    public int[] winSum(int[] nums, int k) {
        // write your code here
        if (nums.length > 0) {
            int[] ret = new int[nums.length - k + 1];
            Arrays.fill(ret, 0);
            for (int i = 0; i < k; ++i) {
                ret[0] += nums[i];
            }
            for (int i = 1; i < nums.length - k + 1; ++i) {
                ret[i] = ret[i - 1] - nums[i - 1] + nums[i + k - 1];
            }
            return ret;
        } else {
            return new int[0];
        }
    }
}