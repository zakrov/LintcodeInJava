// https://www.lintcode.com/problem/judge-route-circle/

public class Solution {
    /**
     * @param moves: a sequence of its moves
     * @return: if this robot makes a circle
     */
    public boolean judgeCircle(String moves) {
        // Write your code here
        int x = 0, y = 0;
        for (int i = 0; i < moves.length(); ++i) {
            char dir = moves.charAt(i);
            if (dir == 'U') {
                --x;
            } else if (dir == 'D') {
                ++x;
            } else if (dir == 'L') {
                --y;
            } else {
                ++y;
            }
        }
        return (x == 0) && (y == 0);
    }
}