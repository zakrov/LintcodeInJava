// https://www.lintcode.com/problem/array-score/

public class Solution {
    /**
     * @param nums: the array to be scored.
     * @param k: the requirement of subarray length.
     * @param u: if the sum is less than u, get 1 score.
     * @param l: if the sum is greater than l, lose 1 score.
     * @return: return the sum of scores for every subarray whose length is k.
     */
    public int arrayScore(List<Integer> nums, int k, long u, long l) {
        // write your code here.
        long ret = 0, sum = 0; // 会溢出，不能用int。
        for (int i = 0; i < k; ++i) {
            sum += nums.get(i);
        }
        if (sum < u) {
            ++ret;
        }
        if (sum > l) {
            --ret;
        }
        for (int i = k; i < nums.size(); ++i) {
            sum += nums.get(i) - nums.get(i - k);
            if (sum < u) {
                ++ret;
            }
            if (sum > l) {
                --ret;
            }
        }
        return (int)ret;
    }
}