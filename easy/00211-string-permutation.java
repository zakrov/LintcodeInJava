// https://www.lintcode.com/problem/string-permutation/

public class Solution {
    /**
     * @param A: a string
     * @param B: a string
     * @return: a boolean
     */
    public boolean Permutation(String A, String B) {
        // write your code here
        if (A.length() != B.length()) {
            return false;
        }
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < A.length(); ++i) {
            char c_a = A.charAt(i), c_b = B.charAt(i);
            if (map.containsKey(c_a)) {
                map.put(c_a, map.get(c_a) + 1);
            }
            else {
                map.put(c_a, 1);
            }
            if (map.containsKey(c_b)) {
                map.put(c_b, map.get(c_b) - 1);
            }
            else {
                map.put(c_b, -1);
            }
        }
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() != 0) {
                return false;
            }
        }
        return true;
    }
}