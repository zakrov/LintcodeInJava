// https://www.lintcode.com/problem/merge-two-sorted-arrays/

public class Solution {
    /**
     * @param A: sorted integer array A
     * @param B: sorted integer array B
     * @return: A new sorted integer array
     */
    public int[] mergeSortedArray(int[] A, int[] B) {
        // write your code here
        int[] ret = new int[A.length+B.length];
        int i = 0, j = 0, k = 0;
        while ((i < A.length) && (j < B.length)) {
            if (A[i] < B[j]) {
                ret[k] = A[i];
                ++i;
            }
            else {
                ret[k] = B[j];
                ++j;
            }
            ++k;
        }
        if (i < A.length) {
            for (int _i = i; _i < A.length; ++_i) {
                ret[k + (_i - i)] = A[_i];
            }
        }
        if (j < B.length) {
            for (int _j = j; _j < B.length; ++_j) {
                ret[k + (_j - j)] = B[_j];
            }
        }
        return ret;
    }
}