// https://www.lintcode.com/problem/minimum-subarray/

public class Solution {
    /*
     * @param nums: a list of integers
     * @return: A integer indicate the sum of minimum subarray
     */
    public int minSubArray(List<Integer> nums) {
        // write your code here
        int ret_plus = 2147483647, ret_minus = 1; // ret_minus处理小于等于0的情况
        int i = 0, sum = 0;
        while (i < nums.size()) {
            sum += nums.get(i);
            if (sum < 0) {
                if (sum < ret_minus) {
                    ret_minus = sum;
                }
            }
            else {
                sum = 0;
            }
            if ((nums.get(i) >= 0) && (nums.get(i) < ret_plus)) {
                ret_plus = nums.get(i);
            }
            ++i;
        }
        return (ret_minus < 0) ? ret_minus : ret_plus;
    }
}