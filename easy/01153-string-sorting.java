// https://www.lintcode.com/problem/string-sorting/

public class Solution {
    /**
     * @param s: string
     * @return: sort string in lexicographical order
     */
    public String sorting(String s) {
        // write your code here
        String[] words = s.split(",");
        Arrays.sort(words);
        return String.join(",", words);
    }
}