// https://www.lintcode.com/problem/valid-triangle/

public class Solution {
    /**
     * @param a: a integer represent the length of one edge
     * @param b: a integer represent the length of one edge
     * @param c: a integer represent the length of one edge
     * @return: whether three edges can form a triangle
     */
    public boolean isValidTriangle(int a, int b, int c) {
        // write your code here
        int max_edge = Math.max(a, Math.max(b, c));
        int min_edge = Math.min(a, Math.min(b, c));
        int mid_edge = a + b + c - max_edge - min_edge;
        return (((min_edge + mid_edge) > max_edge) && ((max_edge - mid_edge) < min_edge));
    }
}