// https://www.lintcode.com/problem/multi-keyword-sort/

public class Solution {
    /**
     * @param array: the input array
     * @return: the sorted array
     */
    public int[][] multiSort(int[][] array) {
        // Write your code here
        // 数组类型坑爹，二重循环...
        for (int i = 0; i < array.length - 1; ++i) {
            for (int j = i + 1; j < array.length; ++j) {
                int[] tmp = array[i];
                if(array[i][1] < array[j][1]) {
                    array[i] = array[j];
                    array[j] = tmp;
                } else if (array[i][1] == array[j][1]) {
                    if(array[i][0] > array[j][0]) {
                        array[i] = array[j];
                        array[j] = tmp;
                    }
                }
            }
        }
        return array;
    }
}