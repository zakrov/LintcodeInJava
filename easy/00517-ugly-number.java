// https://www.lintcode.com/problem/ugly-number/

public class Solution {
    /**
     * @param num: An integer
     * @return: true if num is an ugly number or false
     */
    public boolean isUgly(int num) {
        // write your code here
        if (num <= 0) {
            return false;
        } else if ((num == 1) || (num == 2) || (num == 3) || (num == 5)) {
            return true;
        } else {
            if ((num % 2) == 0) {
                return isUgly(num / 2);
            } else if ((num % 3) == 0) {
                return isUgly(num / 3);
            } else if ((num % 5) == 0) {
                return isUgly(num / 5);
            } else {
                return false;
            }
        }
    }
}