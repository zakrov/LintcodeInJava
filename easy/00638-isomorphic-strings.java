// https://www.lintcode.com/problem/isomorphic-strings/

public class Solution {
    /**
     * @param s: a string
     * @param t: a string
     * @return: true if the characters in s can be replaced to get t or false
     */
    public boolean isIsomorphic(String s, String t) {
        // write your code here
        ArrayList<Integer> sp = buildPattern(s);
        ArrayList<Integer> tp = buildPattern(t);
        return sp.equals(tp);
    }
    
    protected ArrayList<Integer> buildPattern(String s) {
        ArrayList<Integer> pattern = new ArrayList<>();
        HashMap<Character, Integer> map = new HashMap<>();
        int idx = 0;
        for (int i = 0; i < s.length(); ++i) {
            Character c = s.charAt(i);
            if (!map.containsKey(c)) {
                map.put(c, idx);
                idx += 1;
            }
            pattern.add(map.get(c));
        }
        return pattern;
    }
}