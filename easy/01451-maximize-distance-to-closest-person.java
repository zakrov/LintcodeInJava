// https://www.lintcode.com/problem/maximize-distance-to-closest-person/

public class Solution {
    /**
     * @param seats: an array
     * @return: the maximum distance
     */
    public int maxDistToClosest(int[] seats) {
        // Write your code here.
        int prev = -1, head = -1, mid = -1, tail = -1;
        for (int i = 0; i < seats.length; ++i) {
            if (seats[i] != 0) {
                if (prev != -1) {
                    mid = Math.max(mid, i - prev - 1);
                } else {
                    head = i;
                }
                prev = i;
            }
        }
        if (prev != (seats.length - 1)) {
            tail = seats.length - prev - 1; // 因为至少1个1
        }
        return Math.max((mid + 1) / 2, Math.max(head, tail));
    }
}