// https://www.lintcode.com/problem/implement-stack/

public class Stack {
    protected List<Integer> stack;
    int size;
    
    Stack() {
        stack = new ArrayList<>();
        size = 0;
    }
    
    /*
     * @param x: An integer
     * @return: nothing
     */
    public void push(int x) {
        // write your code here
        if (stack.size() <= size) {
            stack.add(x);
        } else {
            stack.set(size, x);
        }
        ++size;
    }

    /*
     * @return: nothing
     */
    public void pop() {
        // write your code here
        --size;
    }

    /*
     * @return: An integer
     */
    public int top() {
        // write your code here
        return stack.get(size - 1);
    }

    /*
     * @return: True if the stack is empty
     */
    public boolean isEmpty() {
        // write your code here
        return size == 0;
    }
}