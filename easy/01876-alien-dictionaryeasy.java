// https://www.lintcode.com/problem/alien-dictionaryeasy/

public class Solution {
    /**
     * @param words: the array of string means the list of words
     * @param order: a string indicate the order of letters
     * @return: return true or false
     */
    public boolean isAlienSorted(String[] words, String order) {
        // 
        String alphabets = "abcdefghijklmnopqrstuvwxyz";
        Map<Character, Character> map = new HashMap<>();
        for (int i = 0; i < order.length(); ++i) {
            map.put(order.charAt(i), alphabets.charAt(i));
        }
        List<String> new_words = new ArrayList();
        for (String word : words) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < word.length(); ++i) {
                sb.append(map.get(word.charAt(i)));
            }
            new_words.add(sb.toString());
        }
        for (int i = 1; i < new_words.size(); ++i) {
            if (new_words.get(i - 1).compareTo(new_words.get(i)) > 0) {
                return false;
            }
        }
        return true;
    }
}