// https://www.lintcode.com/problem/missing-string/

public class Solution {
    /**
     * @param str1: a given string
     * @param str2: another given string
     * @return: An array of missing string
     */
    public List<String> missingString(String str1, String str2) {
        // Write your code here
        List<String> ret = new ArrayList<>();
        HashSet<String> set = new HashSet<>();
        String[] sa_1 = str1.split(" ");
        String[] sa_2 = str2.split(" ");
        for (int i = 0; i < sa_2.length; ++i) {
            set.add(sa_2[i]);
        }
        for (int i = 0; i < sa_1.length; ++i) {
            if (!set.contains(sa_1[i])) {
                ret.add(sa_1[i]);
            }
        }
        return ret;
    }
}