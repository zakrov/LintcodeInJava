// https://www.lintcode.com/problem/1-bit-and-2-bit-characters/

public class Solution {
    /**
     * @param bits: a array represented by several bits. 
     * @return: whether the last character must be a one-bit character or not
     */
    public boolean isOneBitCharacter(int[] bits) {
        // Write your code here
        int i = 0;
        while (i < bits.length) {
            if ((bits.length - 1) == i) {
                return true;
            }
            if (bits[i] == 0) {
                ++i;
            } else {
                i += 2;
            }
        }
        return false;
    }
}