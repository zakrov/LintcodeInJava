// https://www.lintcode.com/problem/lucky-number-eight/

public class Solution {
    /**
     * @param n: count lucky numbers from 1 ~ n
     * @return: the numbers of lucky number
     */
    public int luckyNumber(int n) {
        // Write your code here
        int ret = 0;
        for (int i = 8; i <= n; ++i) {
            int x = i;
            boolean e = false;
            while (x > 0) {
                if ((x % 10) == 8) {
                    e = true;
                    break;
                }
                x /= 10;
            }
            if (e) {
                ++ret;
            }
        }
        return ret;
    }
}