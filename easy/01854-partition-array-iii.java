// https://www.lintcode.com/problem/partition-array-iii/

public class Solution {
    /**
     * @param array: the input array
     * @param k: the sequence length
     * @return: if it is possible, return true, otherwise false
     */
    public boolean partitionArratIII(int[] array, int k) {
        // write your code here
        if ((array.length % k) != 0) {
            return false;
        }
        int n = array.length / k;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i : array) {
            if (!map.containsKey(i)) {
                map.put(i, 0);
            }
            map.put(i, map.get(i) + 1);
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() > n) {
                return false;
            }
        }
        return true;
    }
}