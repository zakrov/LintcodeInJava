// https://www.lintcode.com/problem/range-sum-query-immutable/

class NumArray {
    protected int[] cache;

    public NumArray(int[] nums) {
        cache = new int[nums.length + 1];
        cache[0] = 0;
        for (int i = 0; i < nums.length; ++i) {
            cache[i + 1] = nums[i] + cache[i];
        }
    }
    
    public int sumRange(int i, int j) {
        return cache[j + 1] - cache[i];
    }
}

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray obj = new NumArray(nums);
 * int param_1 = obj.sumRange(i,j);
 */