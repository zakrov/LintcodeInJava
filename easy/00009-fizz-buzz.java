// https://www.lintcode.com/problem/fizz-buzz/

public class Solution {
    /**
     * @param n: An integer
     * @return: A list of strings.
     */
    public List<String> fizzBuzz(int n) {
        // write your code here
        List<String> ret = new ArrayList<>();
        // 一个if肯定用位运算加查偏移量
        for (int i = 1; i <= n; ++i) {
            if (i % 15 == 0) {
                ret.add("fizz buzz");
            }
            else if (i % 5 == 0) {
                ret.add("buzz");
            }
            else if (i % 3 == 0) {
                ret.add("fizz");
            }
            else {
                ret.add(String.valueOf(i));
            }
        }
        return ret;
    }
}