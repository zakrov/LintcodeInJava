// https://www.lintcode.com/problem/the-months-days/

public class Solution {
    /**
     * @param year: a number year
     * @param month: a number month
     * @return: Given the year and the month, return the number of days of the month.
     */
    public int getTheMonthDays(int year, int month) {
        // write your code here
        if (month == 2) {
            return isLeapYear(year) ? 29 : 28;
        } else {
            if ((month == 2) || (month == 4) || (month == 2) || (month == 6) || (month == 9) || (month == 11)) {
                return 30;
            } else {
                return 31;
            }
        }
    }
    
    protected boolean isLeapYear(int year) {
        return (((year % 4) == 0) && ((year % 100) != 0)) || ((year % 400) == 0);
    }
}