https://www.lintcode.com/problem/largest-number-x-which-occurs-x-times/

public class Solution {
    /**
     * @param arr: an array of integers
     * @return: return the biggest value X, which occurs in A exactly X times.
     */
    public int findX(int[] arr) {
        // write your code here
        int ret = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i : arr) {
            if (!map.containsKey(i)) {
                map.put(i, 0);
            }
            map.put(i, map.get(i) + 1);
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int key = entry.getKey();
            if (key == entry.getValue()) {
                if (key > ret) {
                    ret = key;
                }
            }
        }
        return ret;
    }
}