// https://www.lintcode.com/problem/treeplanning/

public class Solution {
    /**
     * @param trees: the positions of trees.
     * @param d: the minimum beautiful interval.
     * @return: the minimum number of trees to remove to make trees beautiful.
     */
    public int treePlanning(int[] trees, int d) {
        // write your code here.
        List<Integer> left = new ArrayList<>();
        left.add(trees[0]);
        int prev = trees[0];
        for (int i = 1; i < trees.length; ++i) {
            if ((trees[i] - prev) >= d) {
                left.add(trees[i]);
                prev = trees[i];
            }
        }
        return trees.length - left.size();
    }
}