// https://www.lintcode.com/problem/palindrome-number/

public class Solution {
    /**
     * @param num: a positive number
     * @return: true if it's a palindrome or false
     */
    public boolean isPalindrome(int num) {
        // write your code here
        int ret = 0, _num = num;
        while (num > 0) {
            ret = ret * 10  + (num % 10);
            num /= 10;
        }
        return ret == _num;
    }
}