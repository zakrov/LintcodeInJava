// https://www.lintcode.com/problem/nth-to-last-node-in-list/

/**
 * Definition for ListNode.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int val) {
 *         this.val = val;
 *         this.next = null;
 *     }
 * }
 */


public class Solution {
    /*
     * @param head: The first node of linked list.
     * @param n: An integer
     * @return: Nth to last node of a singly linked list. 
     */
    public ListNode nthToLast(ListNode head, int n) {
        // write your code here
        ListNode n_head = head;
        for (int i = 0; i < n; ++i) {
            n_head = n_head.next;
        }
        while (n_head != null) {
            head = head.next;
            n_head = n_head.next;
        }
        return head;
    }
}