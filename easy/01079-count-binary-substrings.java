// https://www.lintcode.com/problem/count-binary-substrings/

public class Solution {
    /**
     * @param s: a string
     * @return: the number of substrings
     */
    public int countBinarySubstrings(String s) {
        // Write your code here
        int ret = 0;
        if (s.length() > 1) {
            int i = 0, prev = -1;
            while (i < s.length()) {
                int j = i;
                while ((i < s.length()) && (s.charAt(i) == s.charAt(j))) {
                    ++i;
                }
                int len = i - j; // 连续0/1的长
                if (prev != -1) { // 一段连0/1的长度
                    ret += Math.min(prev, len); // 0011 -> 01, 0011
                }
                prev = len;
            }
        }
        return ret;
    }
}