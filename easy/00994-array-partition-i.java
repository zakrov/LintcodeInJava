// https://www.lintcode.com/problem/array-partition-i/

public class Solution {
    /**
     * @param nums: an array
     * @return: the sum of min(ai, bi) for all i from 1 to n
     */
    public int arrayPairSum(int[] nums) {
        // Write your code here
        int ret = 0;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i += 2) {
            ret += nums[i];
        }
        return ret;
    }
}