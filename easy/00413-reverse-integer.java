// https://www.lintcode.com/problem/reverse-integer/

public class Solution {
    /**
     * @param n: the integer to be reversed
     * @return: the reversed integer
     */
    public int reverseInteger(int n) {
        // write your code here
        if (n == 0) {
            return 0;    
        }
        int ret = 0;
        while(n != 0) {
            if (Math.abs(ret) > (Integer.MAX_VALUE / 10)) { // 溢出判断
                return 0;
            }
            ret = ret * 10 + (n % 10);
            n /= 10;
        }
        return ret;
    }
}