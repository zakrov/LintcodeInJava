// https://www.lintcode.com/problem/k-diff-pairs-in-an-array/

public class Solution {
    /**
     * @param nums: an array of integers
     * @param k: an integer
     * @return: the number of unique k-diff pairs
     */
    public int findPairs(int[] nums, int k) {
        // Write your code here
        int ret = 0;
        if (nums.length >= 2) {
            HashMap<Integer, Integer> map = new HashMap<>();
            for (int i : nums) {
                if (!map.containsKey(i)) {
                    map.put(i, 0);
                }
                map.put(i, map.get(i) + 1);
            }
            k = Math.abs(k);
            HashSet<Integer> used = new HashSet<>();
            for (int i : nums) {
                if (k == 0) {
                    if ((!used.contains(i)) && (map.get(i) > 1)) {
                        ret += 1;
                        used.add(i);
                    }
                } else {
                    if (!used.contains(i)) {
                        if (map.containsKey(i + k)) {
                            ret += 1;
                            used.add(i);
                        }
                    }
                }
            }
        }
        return ret;
    }
}