// https://www.lintcode.com/problem/pioneering-palindrome/

public class Solution {
    /**
     * @param s: A string containing only uppercase and lowercase letters
     * @return: Judge whether it can become a palindrome
     */
    public boolean isPalindrome(String s) {
        // write your code here
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); ++i) {
            Character c = s.charAt(i);
            if (!map.containsKey(c)) {
                map.put(c, 0);
            }
            map.put(c, map.get(c) + 1);
        }
        int odds = 0;
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if ((entry.getValue() % 2) == 1) {
                ++odds;
            }
        }
        return odds <= 1;
    }
}