https://www.lintcode.com/problem/rearrange-a-string-with-integers/

public class Solution {
    /**
     * @param str: a string containing uppercase alphabets and integer digits
     * @return: the alphabets in the order followed by the sum of digits
     */
    public String rearrange(String str) {
        // Write your code here
        List<Character> letters = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        int sum = -1;
        for (int i = 0; i < str.length(); ++i) {
            char c = str.charAt(i);
            if ((c < '0') || (c > '9')) {
                letters.add(c);
            } else {
                if (sum < 0) { // 注意没出现过数字的情况
                    sum = 0;
                }
                sum += (int)(c - '0');
            }
        }
        Collections.sort(letters);
        for (Character c : letters) {
            sb.append(c);
        }
        if (sum >= 0) {
            sb.append(String.valueOf(sum));
        }
        return sb.toString();
    }
}