// https://www.lintcode.com/problem/a-b-problem/

public class Solution {
    /**
     * @param a: An integer
     * @param b: An integer
     * @return: The sum of a and b 
     */
    public int aplusb(int a, int b) {
        // write your code here
        int ret = a ^ b;
        int steps = a & b;
        while (steps > 0) {
            int new_ret = ret;
            int new_steps = steps << 1;
            ret = new_ret ^ new_steps;
            steps = new_ret & new_steps;
        } 
        return ret;
    }
}