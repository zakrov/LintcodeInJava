// https://www.lintcode.com/problem/arranging-coins/

public class Solution {
    /**
     * @param n: a non-negative integer
     * @return: the total number of full staircase rows that can be formed
     */
    public int arrangeCoins(int n) {
        // Write your code here
        int i = 0;
        while (true) {
            if ((n - i) > 0) {
                n -= i;
                ++i;
            } else if ((n - i) == 0) {
                return i;
            } else {
                return i - 1;
            }
        }
    }
}