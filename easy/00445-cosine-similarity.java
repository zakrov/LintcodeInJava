// https://www.lintcode.com/problem/cosine-similarity/

public class Solution {
    /*
     * @param A: An integer array
     * @param B: An integer array
     * @return: Cosine similarity
     */
    public double cosineSimilarity(int[] A, int[] B) {
        // write your code here
        long ss_a = 0, ss_b = 0, ab = 0; // 用int会溢出
        for (int i = 0; i < A.length; ++i) {
            ss_a += A[i] * A[i];
            ss_b += B[i] * B[i];
            ab += A[i] * B[i];
        }
        if (0 == (ss_a * ss_b)) {
            return 2.0;
        }
        return (double)ab / Math.sqrt((double)(ss_a * ss_b));
    }
}